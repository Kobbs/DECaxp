/*
 * Copyright (C) Jonathan D. Belanger 2019-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used to manage main
 *  memory.
 *
 * Revision History:
 *
 *  V01.000 26-Dec-2019 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.002 09-Feb-2020 Jonathan D. Belanger
 *  I'm refactoring this a bit.  First, I'm getting the individual array sizes
 *  straight out of the configuration.  Second, there are going to be between
 *  one and four threads, one per array.  Requests will be queued directly up
 *  to each memory for processing.  This will allow the emulation to function
 *  more like the real thing.
 */
#include "com-util/AXP_Configure.h"
#include "com-util/AXP_SysDataDefs.h"
#include "com-util/AXP_Blocks.h"
#include "com-util/AXP_Trace.h"
#include "memory/AXP_MemoryDefs.h"

/*
 *  Expectations of the Cchip:
 *      1) When writing, the Cchip has already had the Dchip put the data into
 *         its memory write buffers.
 *      2) For partial writes, which are only for DMA writes, the Cchip needs to
 *         get the current memory block, merge in the DNA bytes and then write
 *         out the entire block.  This is a difference from the real hardware,
 *         which can perform partial writes by octawords.
 *      3) When reading, the Cchip has not yet put a request into the Dchip to
 *         get the data out of its memory read buffers.  It will do so,
 *         immediately after calling this function.
 *  If the Cchip does not do the above, then it is very possible that there
 *  will be either no data in the queue for this code to put into memory or the
 *  Dchip will look for a buffer in its Memory Read Buffer that is not there.
 *
 *  Message flows:
 *
 *      Cchip                   Dchip                   Memory
 *      -----------             ----------------------- --------------------
 *      ReadMemory -----------------------------------> Get data from Memory
 *                              Memory Read Buffer <--- Copy to Dchip
 *      ReadMemory -----------> Get Memory Read Buffer
 *
 *      WriteMemory ----------> Put Memory Write Buffer
 *      WriteMemory ----------------------------------> Get Memory Write Buffer
 *                                                      Put data into Memory
 *
 * Design Philosophy:
 *  There is one thread for each memory array, and each array works
 *  asynchronously.  When a memory array is processing a request to read or
 *  write, another read or write can be queued up in the previous requests
 *  shadow.  The array will begin processing the next request as soon as it has
 *  completed the previous.  A call to request something of memory does not
 *  wait for that request to complete.  Instead, the request is queued up to
 *  the target array and processed by the array at its next opportunity.
 *  Requests queued up to an array are process in the same order in which they
 *  were queued.  This way a write to a memory block, followed by a read from
 *  the same memory block will have the read getting the updated data.  A write
 *  to memory does not have a notification mechanism.  Therefore, a write will
 *  happen some time after the request has been submitted.  The only way to
 *  know that a memory write has completed is that the Memory buffer for that
 *  array indicates that it no longer contains valid data.
 */

/*
 * Argument vector to pass to Array threads.
 */
typedef struct
{
    AXP_21274_SYSTEM *sys;
    u32 arrayNum;
} AXP_21274_Memory_Argv;

/*
 * Forward prototype declarations
 */
static void *
AXP_21274_ArrayMain(void *argv);

/*
 * AXP_21274_Addr2Array
 *  This function is called to convert an address to an array index.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21274 system data structure.
 *  address:
 *      This is the 64-bit address to indicate where in memory the data is to
 *      be read from or written to.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  -1 = array not found
 *  >0 = index of the array associated with the supplied address.
 */
int
AXP_21274_Addr2Array(AXP_21274_SYSTEM *sys, u64 address)
{
    int retVal = -1;
    int ii;

    /*
     * We convert an address by determining where the supplied address is
     * inclusively between the base and maximum addresses for an array.  If
     * we don't find one, then we return a -1 index.
     */
    for (ii = 0; ((ii < sys->memory.arrayCount) && (retVal == -1)); ii++)
    {
        if ((sys->memory.arrays[ii].baseAddress <= address) &&
            (sys->memory.arrays[ii].maxAddress >= address))
        {
            retVal = ii;
        }
    }
    return(retVal);
}

/*
 * AXP_21274_Memory_Init
 *  This function is called to allocate the memory for the system.  If the
 *  memory is already allocated, then nothing is done.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21274 system data structure.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true:   The call succeeded.
 *  false:  The call failed to allocate the memory.
 */
bool
AXP_21274_Memory_Init(AXP_21274_SYSTEM *sys)
{
    AXP_21274_Memory_Argv *argv;
    AXP_21274_MemArray *arrays = sys->memory.arrays;
    bool retVal = true;
    u64 baseAddress = 0;
    int ii;

    /*
     * Get the number of arrays and then initialize each array accordingly.
     */
    sys->memory.memSize = 0;
    AXP_ConfigGet_ArrayCount(&sys->memory.arrayCount);
    for (ii = 0; ((ii < sys->memory.arrayCount) && (retVal == true)); ii++)
    {
        AXP_ConfigGet_ArrayInfo(ii, &arrays[ii].arraySize);

        /*
         * Round the memory size up to its nearest valid size.
         */
        if (arrays[ii].arraySize <= 64)
        {
            arrays[ii].arraySize = 64 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 128)
        {
            arrays[ii].arraySize = 128 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 256)
        {
            arrays[ii].arraySize = 256 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 512)
        {
            arrays[ii].arraySize = 512 * ONE_M;
        }
        else if (sys->memory.arrays[ii].arraySize  <= 1024)
        {
            arrays[ii].arraySize = 1024 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 2048)
        {
            arrays[ii].arraySize = 2048 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 4096)
        {
            arrays[ii].arraySize = 4096 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 8192)
        {
            arrays[ii].arraySize = 8192 * ONE_M;
        }
        else if (arrays[ii].arraySize  <= 16384)
        {
            arrays[ii].arraySize = 16384 * ONE_M;
        }
        else
        {
            arrays[ii].arraySize = 32768 * ONE_M;
        }
        sys->memory.memSize += arrays[ii].arraySize;

        /*
         * Now we know how large a specific memory array needs to be, so go
         * allocate it.
         */
        arrays[ii].memBlks = (AXP_21274_MemoryUnit *)
            AXP_Allocate_Block(-arrays[ii].arraySize, NULL);

        /*
         * If we successfully allocated the memory for the array, then
         * initialize the information in the array structure.
         */
        if (arrays[ii].memBlks != NULL)
        {
            arrays[ii].baseAddress = baseAddress;
            baseAddress += arrays[ii].arraySize;
            arrays[ii].maxAddress = baseAddress - sizeof(u128);

            /*
             * Before we can create the new thread, there are a number of
             * mutexes and condition variables that also need to be created.
             */
            pthread_mutex_init(&arrays[ii].toArrayMutex, NULL);
            pthread_cond_init(&arrays[ii].toArrayCond, NULL);
            pthread_mutex_init(&arrays[ii].arrayReadyMutex, NULL);
            pthread_cond_init(&arrays[ii].arrayReadyCond, NULL);
            arrays[ii].arrayReady = false;
            pthread_mutex_init(&arrays[ii].dataReadyMutex, NULL);
            pthread_cond_init(&arrays[ii].dataReadyCond, NULL);
            pthread_mutex_init(&arrays[ii].bufferReadyMutex, NULL);
            pthread_cond_init(&arrays[ii].bufferReadyCond, NULL);
            arrays[ii].dataReady = false;

            /*
             * OK, this memory array is set up, create the request thread for
             * it.
             */
            argv = AXP_Allocate_Block((i32) -sizeof(AXP_21274_Memory_Argv),
                                          NULL);
            if (argv != NULL)
            {
                argv->sys = sys;
                argv->arrayNum = (u32) ii;
            }
            if ((argv == NULL) ||
                (pthread_create(&sys->memory.arrayThreads[ii], NULL,
                               AXP_21274_ArrayMain, (void *) argv)))
            {

                /*
                 * If tracing is turned on, then log the fail attempt to create
                 * a memory array thread to the log file.
                 */
                if (AXP_SYS_OPT1)
                {
                    AXP_TRACE_BEGIN();
                    AXP_TraceWrite("AXP_21274_Memory_Init failed to create "
                            "array thread for memory array #%d", ii);
                    AXP_TRACE_END();
                }
                retVal = false;
            }
        }
        else
        {
            retVal = false;
        }
    }

    /*
     * The only reason we have a failure is because we were unable to allocate
     * all the memory we needed to allocate.  So, before we get out of here, we
     * need to clean up what we did allocate.
     */
    if (retVal == false)
    {
        for (ii = 0; ((ii < sys->memory.arrayCount) &&
                      (arrays[ii].memBlks != NULL)); ii++)
        {
            AXP_Deallocate_Block(arrays[ii].memBlks);
        }

        /*
         * If tracing is turned on, then log the failed return from this call
         * to the log file.
         */
        if (AXP_SYS_OPT1)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("AXP_21274_Memory_Init failed to create %llu MB of "
                    "memory", sys->memory.memSize);
            AXP_TRACE_END();
        }
    }
    else
    {

        /*
         * If tracing is turned on, then log the successful return from this
         * call to the log file.
         */
        if (AXP_SYS_OPT1)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("AXP_21274_Memory_Init created %llu MB of memory",
                           sys->memory.memSize);
            AXP_TRACE_END();
        }
    }

    /*
     * Return back to the caller.
     */
    return (retVal);
}

/*
 * AXP_21274_Memory_Rq
 *  This function is called by the Cchip when there is either data to be read
 *  from or written to.  When this call is made:
 *
 *      1) If we are being asked to write something to memory, then what is to
 *         be written is already sitting in the Dchip memory write buffers.
 *      2) If we are being asked to read something from memory, then we need to
 *         go into memory and get the contents being requested and put this into
 *         the Dchip memory read buffers.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21274 system data structure.
 *  rq:
 *      An enumerated value indicating what the Cchip is requesting of the
 *      memory module.
 *  address:
 *      This is the 64-bit address to indicate where in memory the data is to
 *      be read from or written to.
 *  id:
 *      A pointer to a 64-bit unsigned value containing the identifier assigned
 *      to a block that is read from the the memory buffer into memory.
 *
 * Output Parameters:
 *  id:
 *      A pointer to a 64-bit unsigned value to receive the identifier assigned
 *      to a block that is written into the memory buffer for the Dchip.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Memory_Rq(AXP_21274_SYSTEM *sys, AXP_21274_Mem_Op rq,
                    u64 address, u64 *id)
{
    bool result = false;
    int arrayIdx = AXP_21274_Addr2Array(sys, address);

    /*
     * If tracing is turned on, then log this call to the log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Memory_Rq called to '%s' a block to memory "
                "address 0x%016llx in memory array #%d",
                rq == ReadMem ? "ReadMem" : "WriteMem", address, arrayIdx);
        AXP_TRACE_END();
    }

    /*
     * Using the calculated array index, send either a read or write to it.
     */
    if (arrayIdx >= 0)
    {
        AXP_21274_MemArray *array = &sys->memory.arrays[arrayIdx];

        /*
         * If the system is not powering down and the array is not ready for
         * another request, then wait for it to let us know that it is ready.
         */
        pthread_mutex_lock(&array->arrayReadyMutex);
        while ((sys->state != SysPwrDown) && (array->arrayReady == false))
        {
            pthread_cond_wait(&array->arrayReadyCond, &array->arrayReadyMutex);
        }

        /*
         * We own the request fields for this array, so set the flag that the
         * array is not ready for another request to be queued up.  The array
         * will indicates it's ready when it has removed the request we are
         * sending now.
         */
        array->arrayReady = false;
        pthread_mutex_unlock(&array->arrayReadyMutex);

        /*
         * If the system is not powering down, then queue up a request to the
         * array.
         */
        if (sys->state != SysPwrDown)
        {

            /*
             * Send the request to the appropriate array thread for processing.
             */
            pthread_mutex_lock(&array->toArrayMutex);
            array->request = rq;
            array->address = address;
            array->id = id;
            pthread_cond_signal(&array->toArrayCond);
            pthread_mutex_unlock(&array->toArrayMutex);
            result = true;
        }
    }

    /*
     * If tracing is turned on, then log the return from this call to the
     * log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Memory_Rq '%s' a block %s memory array #%d "
                       "was %squeued",
                       rq == ReadMem ? "ReadMem" : "WriteMem",
                       rq == ReadMem ? "from" : "to", arrayIdx,
                       result ? "" : "not ");
        AXP_TRACE_END();
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Memory_Data_Ready
 *  This function is called to return an indication that the request send to
 *  an array for a read has been completed and that the address of the id
 *  to indicate the semi-unique identifier has been filled in.  This allows the
 *  Cchip to send a read request to the memory array and not wait for the data
 *  to be read out of memory.  When the Dchip gets the request, it is told
 *  which array the request was sent to so that it can call this function and
 *  either get the indicator that the request has been completed, successfully
 *  or not, and that the id it now has is valid.  When this function returns,
 *  the id is valid.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21275 system data structure.
 *  address:
 *      This is the 64-bit address indicating where in memory the data is to
 *      be read from.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true:   Data has been copied into the Dchip memory buffer
 *  false:  The system is shutting down.
 */
bool
AXP_21274_Memory_Data_Ready(AXP_21274_SYSTEM *sys, u64 address)
{
    bool retVal = true;
    int arrayIdx = AXP_21274_Addr2Array(sys, address);

    /*
     * If tracing is turned on, then log the this call to the log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Memory_Data_Ready called to check a ReadMem"
                " to memory address 0x%016llx in memory array #%d has "
                "completed", address, arrayIdx);
        AXP_TRACE_END();
    }

    if (arrayIdx >= 0)
    {
        AXP_21274_MemArray *array = &sys->memory.arrays[arrayIdx];

        /*
         * First we need to lock the appropriate response mutex.
         */
        pthread_mutex_lock(&array->dataReadyMutex);

        /*
         * Loop on the response ready flag until it is set, waiting on the
         * condition variable if it is not yet set.
         */
        while ((sys->state != SysPwrDown) && (array->dataReady == false))
        {
            pthread_cond_wait(&array->dataReadyCond, &array->dataReadyMutex);
        }

        /*
         * If the system is shutting down, let the caller know.
         */
        if (sys->state == SysPwrDown)
        {
            retVal = false;
        }

        /*
         * OK, the flag has been set.  Clear it and then unlock the response
         * mutex.
         */
        pthread_mutex_unlock(&array->dataReadyMutex);
    }

    /*
     * If tracing is turned on, then log the return from this call to the
     * log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Memory_Data_Ready checked a ReadMem"
                       " to memory address 0x%016llx in memory array #%d has"
                       "%s completed", address, arrayIdx, retVal ? "" : " not");
        AXP_TRACE_END();
    }

    /*
     * Return back to the caller.
     */
    return(retVal);
}

/*
 * AXP_21274_Memory_Buffer_Ready
 *  This function is called to let the memory array know that the data in the
 *  memory buffer, where it saves the data associated with a Read request, is
 *  available for the next read.  the Dchip has moved the data out of the
 *  buffer (actually, the entire buffer has been moved and a free one replaced
 *  it).
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21275 system data structure.
 *  address:
 *      This is the 64-bit address indicating where in memory the data is to
 *      be read from.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Memory_Buffer_Ready(AXP_21274_SYSTEM *sys, u64 address)
{
    int arrayIdx = AXP_21274_Addr2Array(sys, address);

    /*
     * If tracing is turned on, then log the this call to the log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Memory_Buffer_Ready called to indicate "
                "the buffer for memory address 0x%016llx in memory array #%d"
                " has been read", address, arrayIdx);
        AXP_TRACE_END();
    }

    if (arrayIdx >= 0)
    {
        AXP_21274_MemArray *array = &sys->memory.arrays[arrayIdx];

        /*
         * First we need to lock the appropriate response mutex.
         */
        pthread_mutex_lock(&array->dataReadyMutex);

        /*
         * Clear the data ready flag, so that the array thread know it is
         * available for the next read.
         */
        array->dataReady = false;

        /*
         * OK, the flag has been set.  Clear it and then unlock the response
         * mutex.
         */
        pthread_mutex_unlock(&array->dataReadyMutex);

        /*
         * The array thread may be waiting on the flag getting cleared, signal
         * it to let it know that it can proceed with another read, if it has
         * one.
         */
        pthread_mutex_lock(&array->bufferReadyMutex);
        pthread_cond_signal(&array->bufferReadyCond);
        pthread_mutex_unlock(&array->bufferReadyMutex);
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Get_Memory_Size
 *  This function is used to get the actual size of the memory for the system.
 *  This function should be called after the initialization.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21274 system data structure.
 *
 * Output Parameters:
 *  memSize:
 *      A pointer to an unsigned 64-bit value to receive the actual size of
 *      main memory.
 *  arrayCount:
 *      A pointer to an unsigned 32-bit value to receive the number of arrays
 *      used in support of the total main memory.
 *
 * Return Value:
 *  None.
 */
void
AXP_21274_Get_Memory_Size(AXP_21274_SYSTEM *sys, u64 *memSize, u32 *arrayCount)
{

    /*
     * If tracing is turned on, then log the return from this call to the
     * log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Get_Memory_Size called to return, "
                "%llu in %d arrays", sys->memory.memSize,
                sys->memory.arrayCount);
        AXP_TRACE_END();
    }

    *memSize = sys->memory.memSize;
    *arrayCount = sys->memory.arrayCount;

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Get_Array_Info
 *  This function is used to get the actual configuration information for the
 *  indicated memory array.  This function should be called after the
 *  initialization and returns information used to initialize the AARx CSRs.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the 21274 system data structure.
 *  arrIdx:
 *      A value indicating for which array information is to be returned.
 *
 * Output Parameters:
 *  arraySize:
 *      A pointer to an unsigned 64-bit value to receive the size for this
 *      particular array.
 *  baseAddress:
 *      A pointer to an unsigned 64-bit value to receive the base address for
 *      this particular array.
 *  rows:
 *      A pointer to an unsigned 32-bit value to receive the number of rows
 *      needed to have the array hold the arraySize.
 *  banks:
 *      A pointer to an unsigned 32-bit value to receive the number of columns
 *      needed to have the array hold the arraySize.
 *  split:
 *  twiceSplit:
 *
 *  NOTE: The rows and banks, along with the array size, split, and twiceSplit,
 *        can be used to determine the number of columns.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Get_Array_Info(AXP_21274_SYSTEM *sys, int arrIdx, u64 *arraySize,
                         u64 *baseAddress, u32 *rows, u32 *banks, u8 *split,
                         u8 *twiceSplit)
{

    /*
     * If the array index we are to be looking into is valid, then get the
     * requested information.
     */
    if (arrIdx < sys->memory.arrayCount)
    {
        *arraySize = sys->memory.arrays[arrIdx].arraySize;
        *baseAddress = sys->memory.arrays[arrIdx].baseAddress;
        *rows = sys->memory.arrays[arrIdx].arraySize < (128 * ONE_M) ? 0 :
                (sys->memory.arrays[arrIdx].arraySize < (2048 * ONE_M) ? 1 : 2);
        *banks = sys->memory.arrays[arrIdx].arraySize < (128 * ONE_M) ? 0 :
                (sys->memory.arrays[arrIdx].arraySize < (4096 * ONE_M) ? 1 : 2);
        *split = sys->memory.arrays[arrIdx].arraySize < (64 * ONE_M) ? 0 : 1;
        *twiceSplit = 0;
    }

    /*
     * If tracing is turned on, then log the return from this call to the
     * log file.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_Get_Array_Info called to return, array %d",
                       arrIdx);
        AXP_TraceWrite("    Array Size = %llu", *arraySize);
        AXP_TraceWrite("    Base Address = 0x%016llx", *baseAddress);
        AXP_TraceWrite("    Rows = %u", *rows);
        AXP_TraceWrite("    Banks = %u", *banks);
        AXP_TraceWrite("    Split: %u", *split);
        AXP_TraceWrite("    Twice Split: %u", *twiceSplit);
        AXP_TRACE_END();
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_ArrayMain
 *  This function is called via pthread_create.  It contains the code to
 *  process a read/write request for a specific array.  This allows memory
 *  access to be multi-threaded by memory array.
 *
 * Input Parameters:
 *  argv:
 *      This is a pointer to void, but actually contains 2 arguments.  The
 *      first is the address of the system data structure and the second is
 *      the number associated with this array.  This can be a value between
 *      0 and 3 (4 arrays max is allowed).
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
static void *
AXP_21274_ArrayMain(void *voidPtr)
{
    AXP_21274_Memory_Argv *argv = (AXP_21274_Memory_Argv *) voidPtr;
    AXP_21274_SYSTEM *sys = (AXP_21274_SYSTEM *) argv->sys;
    u32 arrayNum = argv->arrayNum;
    AXP_21274_MemArray *myArray = &sys->memory.arrays[arrayNum];
    AXP_21274_Mem_Op myRq;
    u64 *myId = NULL;
    u64 myAddr = 0;
    u64 index;
    u32 len;
    bool result;

    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("AXP_21274_ArrayMain: Memory array #%d is running.",
                       arrayNum);
        AXP_TRACE_END();
    }

    /*
     * We no longer need the block that was allocated to pass arguments to this
     * thread.
     */
    AXP_Deallocate_Block(argv);

    /*
     * The array is started in a not ready state.  Now we are ready.
     */
    myArray->arrayReady = true;

    /*
     * We keep looping while the system is not in a Powering Down state.
     */
    while (sys->state != SysPwrDown)
    {

        /*
         * Get the next request off of this array's queue.
         */
        pthread_mutex_lock(&myArray->toArrayMutex);
        while((sys->state != SysPwrDown) && (myArray->request == MemNoOp))
        {
            pthread_cond_wait(&myArray->toArrayCond, &myArray->toArrayMutex);
        }

        /*
         * If the system state is powering down, then go back to the beginning
         * of the loop.
         */
        if (sys->state == SysPwrDown)
        {
            continue;
        }

        /*
         * OK, we have a request to process.  Let's extract the information
         * we need.
         */
        myRq = myArray->request;
        myArray->request = MemNoOp;
        myAddr = myArray->address;
        myArray->address = 0;
        myId = myArray->id;
        myArray->id = NULL;

        /*
         * We have the information we need in local variables, unlock the
         * mutex.
         */
        pthread_mutex_unlock(&myArray->toArrayMutex);

        /*
         * Let any waiting thread know that this array is ready for another
         * request.
         */
        pthread_mutex_lock(&myArray->arrayReadyMutex);
        myArray->arrayReady = true;
        pthread_cond_signal(&myArray->arrayReadyCond);
        pthread_mutex_unlock(&myArray->arrayReadyMutex);

        /*
         * If tracing is turned on, then log the request to the log file.
         */
        if (AXP_SYS_OPT1)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("AXP_21274_ArrayMain #%d called to '%s' a block to "
                    "memory address 0x%016llx in %d memory array", arrayNum,
                    myRq == ReadMem ? "ReadMem" : "WriteMem", myAddr);
            AXP_TRACE_END();
        }

        /*
         * Next, process the request.
         */
        index = (myAddr - myArray->baseAddress) / AXP_COMMON_BLOCK_SIZE;
        switch (myRq)
        {
            case ReadMem:

                /*
                 * For reads, we need to return the id that gets assigned to
                 * the buffer.  In order to do this, we have a mutex and
                 * condition variable to indicate that the write has completed.
                 */
                pthread_mutex_lock(&myArray->dataReadyMutex);
                *myId = AXP_SysData_Add(sys->memoryIn[arrayNum],
                                        myArray->memBlks[index],
                                        AXP_COMMON_BLOCK_SIZE);
                myArray->dataReady = true;
                pthread_cond_signal(&myArray->dataReadyCond);
                pthread_mutex_unlock(&myArray->dataReadyMutex);
                result = *myId != 0;
                break;

            case WriteMem:
                AXP_SysData_Remove(sys->memoryOut[arrayNum], *myId,
                                   myArray->memBlks[index], &len);
                result = len != 0;
                break;

            case MemNoOp:
                break;
        }

        /*
         * If tracing is turned on, then log the return from this call to the
         * log file.
         */
        if (AXP_SYS_OPT1)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("AXP_21274_ArrayMain #%d '%s' a block %s memory %s",
                           arrayNum, myRq == ReadMem ? "ReadMem" : "WriteMem",
                           myRq == ReadMem ? "from" : "to",
                           result ? "was successful" : "failed");
            AXP_TRACE_END();
        }
    }

    /*
     * We are powering down, so log, if necessary, this fact.
     */
    if (AXP_SYS_OPT1)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("Memory array #%d has stopped.", arrayNum);
        AXP_TRACE_END();
    }

    /*
     * We are done here.
     */
    return(NULL);
}

