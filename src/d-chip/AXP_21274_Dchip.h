/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Dchip emulation.
 *
 * Revision History:
 *
 *  V01.000 22-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21274_DCHIP_H_
#define _AXP_21274_DCHIP_H_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_CPM_PAD.h"

/*
 * The following definitions define the number of entries allowed in each of
 * the internal queues.  Most have 8 entries, except for the WMB, which only
 * has 2.
 */
#define AXP_DCHIP_MAX_WMB_BUFS  2
#define AXP_DCHIP_MAX_xPQ_BUFS  8

/*
 * An Alpha AXP system can have 2, 4, or 8 Dchips.  The Dchips communicate with
 * 1 to 4 CPUs, 1 to 4 Memory Arrays, and 1 or 2 PChips, all at the instruction
 * of 1 Cchip.  That means that the Dchips can be moving data around for up to
 * 10 consumers/producers.  A high-level diagram would look like the following:
 *
 *              +-------+   +-------+   +-------+   +-------+
 *              | CPU 0 |   | CPU 1 |   | CPU 2 |   | CPU 3 |
 *              +---+---+   +---+---+   +---+---+   +---+---+
 *                  ^           ^           ^           ^
 *                  |           +---+ +-----+           |
 *                  +-------------+ | | +---------------+
 *                                | | | |           +---------+
 *                                | | | |    +----->+ Array 0 |
 *                                | | | |    |      +---------+
 *                                v v v v    |
 *                              +-+-+-+-+-+  |      +---------+
 *                              |         +<-+  +-->+ Array 1 |
 *  +-------+                   |         +<----+   +---------+
 *  | Cchip + <---------------> +  Dchips |
 *  +-------+                   |         +<----+   +---------+
 *                              |         +<-+  +-->+ Array 2 |
 *                              +--+---+--+  |      +---------+
 *                                 ^   ^     |
 *                                 |   |     |      +---------+
 *                                 |   |     +----->+ Array 3 |
 *                                 |   |            +---------+
 *                           +-----+   +-----+
 *                           v               v
 *                      +----+----+     +----+----+
 *                      | Pchip 0 |     | Pchip 1 |
 *                      +---------+     +---------+
 *
 * As you can see the Dchips are a potential bottleneck.  There is all kinds of
 * data flowing around, within, and through the Dchips.
 *
 * HRM 7.1 Dchip Architecture
 *
 *  The Dchip performs the following functions:
 *
 *  - Implements data flow between the Pchips, CPUs, and memory.
 *  - Shifts data to and from the PADbus, as required
 *  - Provides Pchip queue buffering
 *  - Provides memory data buffering
 *  - Implements data merging for quadword write operations to memory and the
 *    DMA RMW command
 *
 *  The Dchip does not implement ANY of the following:
 *
 *  - Flow control      (this is the responsibility of the Cchip)
 *  - Error detection   (this is the responsibility of the data recipient)
 *  - Error reporting   (same as Error detection)
 *  - Error correction  (same as Error detection)
 *  - Data wrapping     (this is the responsibility of the Cchip)
 *
 * This implementation is going to be thread-safe, but not threaded.  The Cchip
 * will contain 4 Memory Array Request Queues, each of which will be in a
 * separate thread.  Since the Cchip controls all data movement and resource
 * utilization, it will be the only one that will contain multiple threads.
 *
 * A modification from the real architecture will be that the Cchip can
 * actually read and write its own CSR data directly from/to the Dchip.  This
 * is not for performance reasons, as moving the Cchip CSR data around is not
 * performed very often.  The main reason is for simplicity of implementation.
 * The data flows that would be required to get CSR data through the Dchip when
 * using the Pchip, as the real architecture does, would require more Cchip
 * commands to move the CSR data through the Pchip and then onto the Dchip.
 * For what it's worth, the Dchip does not even know what data is CSR data or
 * not.
 */

typedef struct
{

    /*
     * Data buffers for data coming and and going out of the Dchip.
     */
    AXP_SysData_List *memoryIn[AXP_COMMON_MAX_ARRAYS];
    AXP_SysData_List *memoryOut[AXP_COMMON_MAX_ARRAYS];
    AXP_SysData_List *pciIn[AXP_COMMON_MAX_PCHIPS];
    AXP_SysData_List *pciOut[AXP_COMMON_MAX_PCHIPS];
    AXP_SysData_List *cpuIn[AXP_COMMON_MAX_CPUS];
    AXP_SysData_List *cpuOut[AXP_COMMON_MAX_CPUS];
    AXP_SysData_List *cChipIn;
    AXP_SysData_List *cChipOut;

    /*
     * Data buffers internal to the Dchip.
     */
    AXP_SysData_List *wmb;   /* Write Merge Buffer */
    AXP_SysData_List *tpqm;  /* To PCI for DMA only */
    AXP_SysData_List *tpqp;  /* To PCI for PIO write and PTP read or write */
    AXP_SysData_List *fpq;   /* From PCI */

    /*
     * Dchip Registers
     */
    AXP_21274_DSC dsc;              /* Address: 801.b000.0800 */
    AXP_21274_STR str;              /* Address: 801.b000.0840 */
    AXP_21274_DREV dRev;            /* Address: 801.b000.0880 */
    AXP_21274_DSC2 dsc2;            /* Address: 801.b000.08c0 */
} AXP_21274_DCHIP;

#endif /* _AXP_21274_DCHIP_H_ */
