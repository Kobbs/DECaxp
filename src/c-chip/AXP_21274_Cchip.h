/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Cchip emulation.
 *
 * Revision History:
 *
 *  V01.000 18-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.002 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21274_CCHIP_H_
#define _AXP_21274_CCHIP_H_

#include "com-util/AXP_Utility.h"
#include "com-util/AXP_SysData.h"
#include "com-util/AXP_CAPbus.h"

/*
 * An Alpha AXP system can have only 1 Cchip.  The Cchip controls the rest of
 * the system.  It is responsible for maintaining data order and flow control
 * information between itself and the CPUs, Dchip, and PChips.  The CChip does
 * not have a data path from either the CPUs or Pchip.  All data goes through
 * the Dchip.  A high-level diagram would look like the following
 * (from a Cchip perspective):
 *
 *      +---+---+   SysData
 *      | CPUs  +<----------+
 *      +---+---+           |
 *          ^               v
 *          |  CPM/PAD  +---+---+
 *    SysDc | +-------->+ Dchip +<--+
 *          | |         +---+---+   |
 *          v v             ^       |
 *      +-+---+-+           |       |
 *      | Cchip +-------+   | Data  |
 *      +---+---+  Addr |   |       |
 *          ^           v   v       |
 *      CAP |        +--+---+--+    |
 *          |        | Memory  |    |
 *          v        +---------+    |
 *     +----+---+                   |
 *     | Pchips +<------------------+
 *     +--------+       Data
 *
 *  NOTE: The TIGbus hangs off of the Cchip.  The architecture is quite vague
 *        on this.  The TIGbus is responsible for interrupts and the FlashROM.
 *        Interrupts are collected from up to 64 sources and provided to the
 *        Cchip to determine how to interpret them an provide this information
 *        to the CPUs in a consolidated form.  The FlashROM contains the
 *        console firmware, which is loaded by the SROM into main memory, where
 *        the CPUs can execute it.
 *
 * HRM 6.1 Cchip Architecture
 *
 *  The Cchip performs the following functions:
 *
 *      - Accepts requests from the Pchips and CPUs
 *      - Orders the arriving requests as required
 *      - Selects among the requests to issue controls to the DRAMs
 *      - Issues probes to the CPUs as appropriate to the selected requests
 *      - Translates CPU PIO addresses to PCI and CSR addresses
 *      - Issues commands to the Pchip as appropriate to the selected (PIO or
 *        PTP) requests
 *      - Issues responses to the Pchip and CPUs as appropriate to the issued
 *        requests
 *      - Issues controls to the Dchip as appropriate to the DRAM accesses, and
 *        the probe and Pchip responses
 *      - Controls the TIGbus to manage interrupts, and maintains CSRs
 *        including those that represent interrupt status
 *
 * As you can see, the Cchip is the center of the world, as far as the system
 * is concerned.  The Cchip communicates with all other parts of the system,
 * directly or indirectly.
 */

/*
 * The following definitions are used for the bit vectors, addrMatchWait,
 * pageHit, and olderRqs.  Each bit represents an entry in the queue of 6
 * entries.
 *
 *  Note:   Although there are conceptually 3-bit vectors, they can be combined
 *          into a 2-bit vector to represent the following four comparisons
 *          against each of the other requests in the array queue (address
 *          match wait includes and overrides page hit):
 *
 *      1. Not younger
 *      2. Younger and page hit match (but not address match wait)
 *      3. Younger and address match wait
 *      4. Younger and no match
 */
#define AXP_21274_NOT_YOUNG     0x00
#define AXP_21274_YOUNG_HIT     0x01
#define AXP_21274_YOUNG_WAIT    0x02
#define AXP_21274_YOUNG         0x03
#define AXP_21274_AGE_MASK      0x03

/*
 * This macro extracts the requested 2 bits used to determine the combination
 * of Address Match Wait, Page Hit, and Older Request vectors.
 */
#define AXP_21264_ENTRY(bitVector, entry)   \
  (((bitVecotr) >> ((entry * 2)) & AXP_21274_AGE_MASK)

#endif /* _AXP_21274_CCHIP_H_ */
