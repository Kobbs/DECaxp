/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 07-Mar-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "com-util/AXP_CAPbus.h"

/*
 * AXP_Snd_Msg_To_Pchip
 *  This function is called by the Cchip to send a CAP command to the specified
 *  Pchip.  We use this function so that the Cchip does not need to know about
 *  the inner-workings of the Pchip.
 *
 * Input Parameters:
 *  pchip:
 *      This is the address of the Pchip data structure containing the
 *      information needed by the Pchip to maintain the emulation.
 *  cmd:
 *      This is an enumeration containing the CAPbus command being requested.
 *  ...:
 *      These are variable arguments, based off of the cmd parameter and can
 *      be one of the two following sets of parameters:
 *
 *          - 1 Cycle Commands:
 *              ldp:
 *                  An enumeration indicating the Load PADbus operation to be
 *                  performed.
 *              c:
 *                  An enumeration indicating the which chip's, Pchip or Cchip,
 *                  CSR to which the following parameter refers.
 *              csrNumber:
 *                  The number associated with the CSR to be either read or
 *                  written.
 *          - 2 Cycle Commands:
 *              t:
 *                  An enumeration indicating the data type (granularity) of
 *                  the transfer (byte, longword, or quadword).
 *              mask:
 *                  A value denoting which data is valid in the transfer, and
 *                  is aligned to eight times the data type (granularity).
 *              address:
 *                  A 32-bit value, with the first four bits containing a value
 *                  of 0, representing a PCI address.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void AXP_Snd_Msg_To_Pchip(AXP_21274_PCHIP *pchip,
                          AXP_CAP_Command cmd, ...)
{
    AXP_CAP_Msg msg;
    va_list ap;

    msg.cmd = cmd;
    switch(cmd)
    {

        /*
         * 2 Cycle commands
         */
        case CAP_PCI_IACK_cycle:
        case CAP_PCI_special_cycle:
        case CAP_PCI_IO_read:
        case CAP_PCI_IO_write:
        case CAP_PCI_memory_write_PTP:
        case CAP_PCI_memory_read:
        case CAP_PCI_memory_write_CPU:
        case CAP_PCI_configuration_read:
        case CAP_PCI_configuration_write:
            {
                va_start(ap, cmd);
                msg.t = va_arg(ap, AXP_CAP_T);
                msg.mask = va_arg(ap, AXP_SysDc_Mask);
                msg.address = va_arg(ap, u64);
                va_end(ap);
            }
            break;

        /*
         * 1 Cycle commands
         */
        case CAP_CSR_read:
        case CAP_CSR_write:
        case CAP_Load_PADbus_data_downstream:
            {
                va_start(ap, cmd);
                msg.ldp = va_arg(ap, AXP_CAP_LDP);
                msg.c = va_arg(ap, AXP_CAP_C_Bit);
                msg.csr = va_arg(ap, u16);
                va_end(ap);
            }
            break;

        /*
         * We can ignore everything else.
         */
        case CAP_No_op:
        default:
            return;
    }

    /*
     * TODO: Queue the message up to the Pchip for processing.
     */

    /*
     * Return back to the Cchip.
     */
    return;
}

/*
 * AXP_Snd_Msg_To_Cchip
 *  This function call sends a CAPbus message to the Cchip.
 *
 * Input Parameters:
 *  sys:
 *      This is the address of the System data structure, containing the
 *      information needed by the Cchip to maintain the emulation.
 *  pchipId:
 *      This is the identifier assigned to the Pchip and can be either a value
 *      of 0 or 1.
 *  cmd:
 *      This is an enumeration containing the CAPbus command being requested.
 *  t:
 *      An enumeration indicating the data type (granularity) of the transfer
 *      (byte, longword, or quadword).
 *  mask:
 *      A value denoting which data is valid in the transfer, and is aligned to
 *      eight times the data type (granularity).
 *  address:
 *      A 32-bit value, with the first four bits containing a value of 0,
 *      representing a PCI address.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void AXP_Snd_Msg_To_Cchip(AXP_21274_SYSTEM *sys, u32 pchipId,
                          AXP_CAP_Command cmd, AXP_CAP_T t, AXP_SysDc_Mask mask,
                          u64 address)
{
    AXP_CAP_Msg msg;

    msg.cmd = cmd;
    switch(cmd)
    {
        case CAP_DMA_read_N_QW:
        case CAP_SG_table_entry_read_N_QW:
        case CAP_PTP_memory_read:
        case CAP_PTP_memory_write:
        case CAP_DMA_RMW_QW:
        case CAP_DMA_write_N_QW:
        case CAP_Load_PADbus_data_upstream:
        case CAP_PTP_write_byte_mask_bypass:
            msg.t = t;
            msg.c = pchipId;    /* This is not in the architecture */
            msg.mask = mask;
            msg.address = address;
            break;

        case CAP_No_op:
        default:
            return;
    }

    /*
     * TODO: Queue the message up to the Cchip for processing.
     */

    /*
     * Return back to the Pchip that made the call.
     */
    return;
}
