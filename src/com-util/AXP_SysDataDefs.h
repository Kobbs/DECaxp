/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions for the interface to the
 *  System Data functionality.
 *
 * Revision History:
 *
 *  V01.000 27-Jan-2018 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_SYSDATADEFS_H_
#define _AXP_SYSDATADEFS_H_ 1

#include "com-util/AXP_SysData.h"

/*
 * AXP_SysData_Create
 *  This function is called to create a fully populated SysData List.
 *
 * Input Parameters:
 *  size:
 *      An integer indicating the number of available SysData blocks.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  !NULL   - a newly allocated and initialize SysData List.
 *  NULL    - an error occurred and a SysData List was not allocated.
 */
AXP_SysData_List *
AXP_SysData_Create(int size);

/*
 * AXP_SysData_Delete
 *  This function is called to delete a previously created SysData List.
 *  NOTE: This code does not worry about the fact that a buffer may still be
 *        marked as valid.
 *
 * Input Parameters:
 *  sysdata:
 *      A pointer to the address of a SysData List that was allocated by a call
 *      to AXP_SysData_Create.
 *
 * Output Parameters:
 *  sysdata:
 *      The pointer is reset to NULL.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysData_Delete(AXP_SysData_List **sysdata);

/*
 * AXP_SysData_Add
 *  This function is called to add a block to the SysData List.  The buffer is
 *  copied into a free SysData block.
 *
 * Input Parameters:
 *  sysdata:
 *      A pointer to a SysData List where the buffer is to be copied.
 *  buffer:
 *      A pointer to a buffer to be copied into the SysData block.
 *  length:
 *      A value indicating the length, in bytes, of the buffer.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  0>  - A reasonably unique identifier for the block just copied into a
 *        SysData block.
 *  0   - There was no free SysData block.  The code should error out.
 */
u64
AXP_SysData_Add(AXP_SysData_List *sysdata, u64 *buffer, u32 length);

/*
 * AXP_SysData_Remove
 *  This function is called to remove a block from the SysData List.  The
 *  SysData block is copied into the supplied buffer and length set
 *  accordingly.
 *
 * Input Parameters:
 *  sysdata:
 *      A pointer to a SysData List where the SysData block to be "freed" can
 *      be found.
 *  id:
 *      The identifier associated with the SysData block to be "freed".
 *
 * Output Parameters:
 *  buffer:
 *      A pointer to a buffer to receive the data from the SysData block.  This
 *      buffer must be
 *  length:
 *      A pointer to an unsigned 32-bit integer to receive the number of bytes
 *      actually copied into the buffer.
 *
 * Return Value:
 *  None.
 */
void
AXP_SysData_Remove(AXP_SysData_List *sysdata, u64 id, u64 *buffer, u32 *length);

/*
 * AXP_SysData_Swap
 *  This function is called to move an identified SysData block from a source
 *  list to a destination list and, at the same time, move a free SysData block
 *  from the destination list to the source list.  This way the list always
 *  contain the same number of SysData blocks.
 *
 * Input Parameters:
 *  src_sysdata:
 *      A pointer to the source SysData List containing the block to be swapped
 *      with a "free" block in the destination SysData List.
 *  id:
 *      The identifier associated with the source SysData block to be swapped
 *      into the destination SysData List.
 *  dst_sysdata:
 *      A pointer to the destination SysData List to receive the block from the
 *      source SysData List and give back a "free" block at the same time.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true    - the call did what it was requested to do.
 *  false   - the call was unable to perform the request.
 */
bool
AXP_SysData_Swap(AXP_SysData_List *src_sysdata, u64 id,
                 AXP_SysData_List *dst_sysdata);

#endif /* _AXP_SYSDATADEFS_H_ */
