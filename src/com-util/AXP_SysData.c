/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source module contains the code used to move system data around.
 *  There are a few functions provided by this interface:
 *
 *      1) Create - create a data source/target which contains the information
 *         and all allocated buffers needed to add system data to and be able
 *         to move it around the rest of the system.
 *      2) Delete - delete a data source/target.
 *      3) Add - add a new data source into the first available buffer (there
 *         should always be at least one - if not fatal error).
 *      4) Swap - move data from a data source to a data target.
 *      5) Mark a data item as free.
 *
 * Revision History:
 *
 *  V01.000 25-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 02-Feb-2020 Jonathan D. Belanger
 *  Decided to refactor this code to use pseudo-doubly linked lists.  This
 *  makes swapping a whole lot simpler and faster.
 *
 *  V01.002 09-Feb-2020 Jonathan D. Belanger
 *  I have been having issues with the pthread_once functionality when
 *  compiling with full optimization.  Therefore, I have changed the way the
 *  initialization occurs, from dynamic to static.
 */
#include "com-util/AXP_SysData.h"
#include "com-util/AXP_Blocks.h"

static pthread_mutex_t _sysdata_mutex = PTHREAD_MUTEX_INITIALIZER;
static u64 _sysdata_block_id = 1;

#ifndef _AXP_SYSDATADEFS_H_
void
AXP_SysData_Delete(AXP_SysData_List **p_sysdata);
#endif


/*
 * _axp_sysdata_get_id
 *  This static function is called to get a unique id and make sure that the
 *  id wraps around 0 properly.
 *
 * Input Parameters:
 *  None.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  0>  - Always.
 */
static u64 _axp_sysdata_get_id()
{
    u64 retVal;
    /*
     * First things first, get the unique id to be associated with the entry
     * we are about to use.  We may throw it away if some other error occurs.
     * Also, increment the source of the unique id to the next available one,
     * making sure to write around 0 correctly.  This code needs to be single
     * threaded.
     */
    pthread_mutex_lock(&_sysdata_mutex);
    retVal = _sysdata_block_id;
    _sysdata_block_id++;
    if (_sysdata_block_id == 0)
    {
        _sysdata_block_id++;
    }
    pthread_mutex_unlock(&_sysdata_mutex);

    /*
     * Return back to the caller.
     */
    return(retVal);
}

/*
 * AXP_SysData_Create
 *  This function is called to create a fully populated SysData List.
 *
 * Input Parameters:
 *  size:
 *      An integer indicating the number of available SysData blocks.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  !NULL   - a newly allocated and initialize SysData List.
 *  NULL    - an error occurred and a SysData List was not allocated.
 */
AXP_SysData_List *
AXP_SysData_Create(int size)
{
    AXP_SysData_List *retVal = NULL;

    /*
     * The list size must be at least one.
     */
    if (size > 0)
    {
        i32 allocSize = sizeof(AXP_SysData_List);

        /*
         * First things first, we need to allocate the SysData List structure
         * itself.
         */
        retVal = AXP_Allocate_Block(-allocSize, NULL);
        if (retVal != NULL)
        {
            AXP_SysData_Block *next, *prev = NULL;
            int ii;
            bool ok = true;

            allocSize = sizeof(AXP_SysData_Block);

            /*
             * For each entry allocate a separate block (this will allow
             * moving addresses from one list to another).  If we fail to
             * allocate a block, stop allocating and we'll free up
             * everything allocated thus far.
             */
            for (ii = 0; ((ii < size) && (ok == true)); ii++)
            {
                next = AXP_Allocate_Block(-allocSize, NULL);

                /*
                 * If an entry was allocated, then initialize it to not
                 * valid.  The other fields are don't-cares.  If we failed
                 * to allocate an entry, then set a flag to indicate this
                 * and indicate the number of blocks we tried allocating
                 * (the call to AXP_SysData_Delete will deallocate
                 * everything allocated to that point).
                 */
                if (next != NULL)
                {
                    next->valid = false;
                    if (prev != NULL)
                    {
                        prev->flink = next;
                        next->blink = prev;
                    }
                    else
                    {
                        retVal->first = next;
                        next->blink = (AXP_SysData_Block *)&retVal->first;
                    }
                    prev = next;
                    retVal->list_size++;
                }
                else
                {
                    ok = false;
                }
            }

            /*
             * If we failed to allocate a list entry, then call Delete to
             * deallocate everything allocated in here.
             */
            if (ok == false)
            {
                AXP_SysData_Delete(&retVal);
            }
            else
            {

                /*
                 * Allocate the mutex and condition variable used to protect
                 * the SysData List and let a waiter know that the block has
                 * become free.
                 */
                pthread_mutex_init(&retVal->list_mutex, NULL);
                pthread_cond_init(&retVal->list_cond, NULL);

                /*
                 * Finish initializing the structure.
                 */
                retVal->first_free = retVal->first;
                retVal->id = _axp_sysdata_get_id();
            }
        }
    }

    /*
     * Return back to the caller.
     */
    return(retVal);
}

/*
 * AXP_SysData_Delete
 *  This function is called to delete a previously created SysData List.
 *  NOTE: This code does not worry about the fact that a buffer may still be
 *        marked as valid.
 *
 * Input Parameters:
 *  p_sysdata:
 *      A pointer to the address of a SysData List that was allocated by a call
 *      to AXP_SysData_Create.
 *
 * Output Parameters:
 *  sysdata:
 *      The pointer is reset to NULL.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysData_Delete(AXP_SysData_List **p_sysdata)
{

    /*
     * If the parameter is NULL or its contents  are NULL, then there is
     * nothing to do here.  Otherwise, we have something to deallocate.
     */
    if ((p_sysdata != NULL) && (*p_sysdata != NULL))
    {
        AXP_SysData_List *sysdata = *p_sysdata;

        /*
         * OK, we have a list and the max_blocks value indicates how many
         * we could have.
         */
        while(sysdata->first != NULL)
        {
            AXP_SysData_Block *dealloc = sysdata->first;

            /*
             * Pull the first block out of the list and then deallocate it.
             */
            sysdata->first = dealloc->flink;
            if (sysdata->first != NULL)
            {
                sysdata->first->blink = (AXP_SysData_Block *) &sysdata->first;
            }
            AXP_Deallocate_Block(dealloc);
        }

        /*
         * If the mutex was allocated, destroy it now.
         */
        if (sysdata->id > 0)
        {
            pthread_mutex_destroy(&sysdata->list_mutex);
        }

        /*
         * Deallocate the SysData List and set the supplied address to NULL.
         */
        AXP_Deallocate_Block(sysdata);
        *p_sysdata = NULL;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_SysData_Add
 *  This function is called to add a block to the SysData List.  The buffer is
 *  copied into a free SysData block.
 *
 * Input Parameters:
 *  sysdata:
 *      A pointer to a SysData List where the buffer is to be copied.
 *  buffer:
 *      A pointer to a buffer to be copied into the SysData block.
 *  length:
 *      A value indicating the length, in bytes, of the buffer.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  0>  - A reasonably unique identifier for the block just copied into a
 *        SysData block.
 *  0   - There was no free SysData block.  The code should error out.
 */
u64
AXP_SysData_Add(AXP_SysData_List *sysdata, u64 *buffer, u32 length)
{
    u64 retVal = 0;

    /*
     * If we are trying to add a buffer that is  greater than zero but not too
     * long, then there is nothing more to do.
     */
    if ((length > 0) && (length <= AXP_SYSDATA_BUFLEN))
    {
        AXP_SysData_Block *ptr = sysdata->first_free;
        int ii;

        retVal = _axp_sysdata_get_id();

        /*
         * OK, we have an id, now we need to lock the list itself, as the next
         * body of code also needs to be single threaded, but not globally.
         */
        pthread_mutex_lock(&sysdata->list_mutex);
        if (sysdata->first_free == NULL)
        {
            while (sysdata->first_free)
            {
                pthread_cond_wait(&sysdata->list_cond, &sysdata->list_mutex);
            }
            ptr = sysdata->first_free;
        }

        for(ii = 0; ii < AXP_SYSDATA_BLOCK_LEN; ii++)
        {
            ptr->buffer[ii] = buffer[ii];
        }
        ptr->id = retVal;
        ptr->valid = true;
        ptr->length = length;
        sysdata->first_free = ptr->flink;

        /*
         * Finally, we need to make sure to unlock the mutex.
         */
        pthread_mutex_unlock(&sysdata->list_mutex);
    }

    /*
     * Return back to the caller.
     */
    return(retVal);
}

/*
 * AXP_SysData_Remove
 *  This function is called to remove a block from the SysData List.  The
 *  SysData block is copied into the supplied buffer and length set
 *  accordingly.
 *
 * Input Parameters:
 *  sysdata:
 *      A pointer to a SysData List where the SysData block to be "freed" can
 *      be found.
 *  id:
 *      The identifier associated with the SysData block to be "freed".
 *
 * Output Parameters:
 *  buffer:
 *      A pointer to a buffer to receive the data from the SysData block.  This
 *      buffer must be
 *  length:
 *      A pointer to an unsigned 32-bit integer to receive the number of bytes
 *      actually copied into the buffer.
 *
 * Return Value:
 *  None.
 */
void
AXP_SysData_Remove(AXP_SysData_List *sysdata, u64 id, u64 *buffer, u32 *length)
{
    AXP_SysData_Block *ptr = sysdata->first;
    bool signalWaiter = sysdata->first_free == NULL;

    /*
     * Setting the returned length to zero will indicate that nothing was
     * copied.  We'll only change this after doing the actual copy to the
     * callers buffer.  The caller should gracefully handle this (possibly
     * exiting).
     */
    *length = 0;

    /*
     * OK, we have an id, now we need to lock the list itself, as the next
     * body of code also needs to be single threaded, but not globally.
     */
    pthread_mutex_lock(&sysdata->list_mutex);

    /*
     * Search through each block trying to locate where we are supposed to
     * remove the data.
     */
    while (ptr != NULL)
    {
        if ((ptr->id == id) && (ptr->valid == true))
        {
            int ii;

            /*
             * Before we do anything, copy the data out of the block and
             * into the callers buffer.
             */
            for(ii = 0; ii < AXP_SYSDATA_BLOCK_LEN; ii++)
            {
                buffer[ii] = ptr->buffer[ii];
            }
            *length = ptr->length;
            ptr->valid = false;

            /*
             * Now move the newly freed block to the beginning of the free
             * list, if it is not already there.
             */
            if ((ptr->flink == NULL) || (ptr->flink = sysdata->first_free))
            {
                sysdata->first_free = ptr;
            }
            else
            {
                AXP_SysData_Block *last = sysdata->first_free;

                /*
                 * OK, find the end of the list.
                 */
                while (last->flink != NULL)
                {
                    last = last->flink;
                }

                /*
                 * Pull the newly freed buffer out of the list.
                 */
                if (ptr->blink != (AXP_SysData_Block *) &sysdata->first)
                {
                    ptr->blink->flink = ptr->flink;
                }
                else
                {
                    sysdata->first = ptr->flink;
                }
                if (ptr->flink != NULL)
                {
                    ptr->flink->blink = ptr->blink;
                }

                /*
                 * Make the newly freed buffer the last buffer in the list.
                 */
                last->flink = ptr;
                ptr->blink = last;
                ptr->flink = NULL;
            }

            /*
             * If there were no free buffers, then signal any potential
             * waiters.
             */
            if (signalWaiter == true)
            {
                pthread_cond_signal(&sysdata->list_cond);
            }
            ptr = NULL;
        }

        /*
         * Since the list is sorted by id, if the id we are looking for is less
         * that the id for the current element we are analyzing, then look at
         * the next element.  Otherwise, we are done looking.
         */
        else if (ptr->id < id)
        {
            ptr = ptr->flink;
        }
        else
        {
            ptr = NULL;
        }
    }

    /*
     * Finally, we need to make sure to unlock the mutex.
     */
    pthread_mutex_unlock(&sysdata->list_mutex);

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_SysData_Swap
 *  This function is called to move an identified SysData block from a source
 *  list to a destination list and, at the same time, move a free SysData block
 *  from the destination list to the source list.  This way the list always
 *  contain the same number of SysData blocks.
 *
 * Input Parameters:
 *  src_sysdata:
 *      A pointer to the source SysData List containing the block to be swapped
 *      with a "free" block in the destination SysData List.
 *  id:
 *      The identifier associated with the source SysData block to be swapped
 *      into the destination SysData List.
 *  dst_sysdata:
 *      A pointer to the destination SysData List to receive the block from the
 *      source SysData List and give back a "free" block at the same time.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true    - the call did what it was requested to do.
 *  false   - the call was unable to perform the request.
 */
bool
AXP_SysData_Swap(AXP_SysData_List *src_sysdata, u64 id,
                 AXP_SysData_List *dst_sysdata)
{
    bool retVal = false;

    /*
     * The following is for deadlock avoidance.  We always lock SysData
     * Lists in ID order.  We'll unlock them in the same way.
     */
    if (src_sysdata->id < dst_sysdata->id)
    {
        pthread_mutex_lock(&src_sysdata->list_mutex);
        pthread_mutex_lock(&dst_sysdata->list_mutex);
    }
    else
    {
        pthread_mutex_lock(&dst_sysdata->list_mutex);
        pthread_mutex_lock(&src_sysdata->list_mutex);
    }

    /*
     * Make sure the destination list has a free block.  Otherwise, we have
     * an error.
     */
    if (dst_sysdata->first_free != NULL)
    {
        AXP_SysData_Block *src = src_sysdata->first;
        AXP_SysData_Block *dst = dst_sysdata->first_free;

        /*
         * Now go look for the block in the source SysData list.
         */
        while (src != NULL)
        {
            if ((src->id == id) && (src->valid == true))
            {
                AXP_SysData_Block *last;

                /*
                 * First things first, pull the free block out of the
                 * destination list and the source block out of the source
                 * list.
                 */
                if (dst->blink != (AXP_SysData_Block *) &dst_sysdata->first)
                {
                    dst->blink->flink = dst->flink;
                }
                else
                {
                    dst_sysdata->first = dst->flink;
                }
                if (dst->flink != NULL)
                {
                    dst->flink->blink = dst->blink;
                }
                if (src->blink != (AXP_SysData_Block *) &src_sysdata->first)
                {
                    src->blink->flink = src->flink;
                }
                else
                {
                    src_sysdata->first = src->flink;
                }
                if (src->flink != NULL)
                {
                    src->flink->blink = src->flink;
                }

                /*
                 * Second, find the end of the source list.
                 */
                last = src_sysdata->first;
                while ((last != NULL) && (last->flink != NULL))
                {
                    last = last->flink;
                }

                /*
                 * Third, put the free block just pulled out of the destination
                 * list and put it at the end of the source list.
                 */
                dst->flink = NULL;
                if (last != NULL)
                {
                    last->flink = dst;
                    dst->blink = last;
                }
                else
                {
                    src_sysdata->first = dst;
                    src_sysdata->first_free = dst;
                    dst->blink = (AXP_SysData_Block *) &src_sysdata->first;
                }

                /*
                 * Finally, what we really came to do here, insert the source
                 * block into the correct location within the destination list.
                 */
                last = dst_sysdata->first;
                while (last != NULL)
                {
                    if (src->id < last->id)
                    {
                        src->flink = last;
                        src->blink = last->blink;
                        if (dst_sysdata->first_free == NULL)
                        {
                            dst_sysdata->first_free = src;
                        }
                        if (last->blink ==
                            (AXP_SysData_Block *) &dst_sysdata->first)
                        {
                            dst_sysdata->first = src;
                        }
                        else
                        {
                            last->blink->flink = src;
                        }
                        last->blink = src;
                        last = NULL;
                    }
                    else
                    {
                        last = last->flink;
                    }
                }
                src = NULL;
                retVal = true;
            }
            else if (src->id < id)
            {
                src = src->flink;
            }
            else
            {
                src = NULL;
            }
        }
    }

    /*
     * Finally, we need to make sure to unlock the mutex.  We do it based
     * on ID to avoid a deadlock.
     */
    if (src_sysdata->id < dst_sysdata->id)
    {
        pthread_mutex_unlock(&src_sysdata->list_mutex);
        pthread_mutex_unlock(&dst_sysdata->list_mutex);
    }
    else
    {
        pthread_mutex_unlock(&dst_sysdata->list_mutex);
        pthread_mutex_unlock(&src_sysdata->list_mutex);
    }

    /*
     * Return back to the caller.
     */
    return(retVal);
}

