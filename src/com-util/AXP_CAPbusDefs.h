/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 29-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _AXP_CAPBUSDEFS_H_
#define _AXP_CAPBUSDEFS_H_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_CAPbus.h"
#include "system/AXP_21274_System.h"

/*
 * For the following 2 function prototypes, the arguments to be supplied depend
 * upon the command being supplied.  A call to AXP_CAPbus_Cmd_Type is made
 * prior to the call to send the message.  The following 5 or 6 parameters must
 * be supplied on the call, depending upon the cmd, in order:
 *
 *      AXP_21274_PCHIP *pchip,
 *      AXP_CAP_Command cmd,
 *      AXP_CAP_LDP ldp,
 *      AXP_CAP_B_Bit c,
 *      u16 csrNumber
 *
 * If the Cmd_Type returns a value of 2, the following 5 parameters must be
 * supplied on the call, in order:
 *
 *      AXP_21274_PCHIP *pchip,
 *      AXP_CAP_Command cmd,
 *      AXP_CAP_T t,
 *      AXP_SysDc_Mask mask,
 *      u64 address
 *
 * This function call sends a CAPbus message to the indicated Pchip.
 */
void AXP_Snd_Msg_To_Pchip(AXP_21274_PCHIP *pchip,
                          AXP_CAP_Command cmd, ...);

/*
 * This function call sends a CAPbus message to the Cchip
 */
void AXP_Snd_Msg_To_Cchip(AXP_21274_SYSTEM *sys, u32 pchipId,
                          AXP_CAP_Command cmd, AXP_CAP_T t, AXP_SysDc_Mask mask,
                          u64 address);

#endif /* _AXP_CAPBUSDEFS_H_ */
