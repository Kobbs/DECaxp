/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 07-Mar-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_CPM_PAD_H_
#define _AXP_CPM_PAD_H_

#include "com-util/AXP_Common_Include.h"

/*
 * Table 7-2 PADbus Command Encodings
 * ----------------------------------------------------------------------------
 * VCCT     Mnemonic    Command
 * ----------------------------------------------------------------------------
 * 0xxx     ---         No-op
 * 1000     P-FPQ       Move data from the Pchip to the Dchips
 * 1001     TPQM-P      Move data to the Pchip from the Dchip's TPQM
 * 1010     P-WMB       Return data from Pchip to Dchips for RMW
 * 1011     WMB-P       Move data from Dchips to Pchip for RMW
 * 1100     PP-FPQ      Stutter move of data from the Pchip to the Dchips
 * 1101     TPQP-P      Move data to the Pchip from the Dchip's TPQP
 * 111x     ---         Reserved
 * ----------------------------------------------------------------------------
 *
 * The special "stutter" command is used for PIO read byte and PIO read
 * longword operations from a CPU. In these cases, the transfer to the CPU must
 * have each quadword sent twice in succession. To accomplish this, each
 * quadword from the Pchip is written into two successive locations in the FPQ
 * when the PP�FPQ command is received. Then, a normal CPM command is used to
 * transfer the data from the FPQ to the CPU.
 */
typedef enum
{
    PAD_NoOp0 = 0,          /* b'0000' */
    PAD_NoOp1 = 1,          /* b'0001' */
    PAD_NoOp2 = 2,          /* b'0010' */
    PAD_NoOp3 = 3,          /* b'0011' */
    PAD_NoOp4 = 4,          /* b'0100' */
    PAD_NoOp5 = 5,          /* b'0101' */
    PAD_NoOp6 = 6,          /* b'0110' */
    PAD_NoOp7 = 7,          /* b'0111' */
    PAD_P_FPQ = 8,          /* b'1000' */
    PAD_TPQM_P = 9,         /* b'1001' */
    PAD_P_WMB = 10,         /* b'1010' */
    PAD_WMB_P = 11,         /* b'1010' */
    PAD_PP_FPQ = 12,        /* b'1011' */
    PAD_TPQP_P = 13         /* b'1101' */
} AXP_PAD_Command;

/*
 * HRM 7.3.1 Dchip-PADbus Interface Control --- PAD Commands
 *
 * The Cchip issues PADbus commands to the Dchips to control the movement of
 * data between the Pchips and the Dchips. Data from a Pchip is loaded into the
 * FPQ, and data to the Pchips is unloaded from the TPQ. The two-phase command
 * encoding is shown in Table 7-1.
 *
 *  Table 7-1 PADbus Command Format
 *
 *            4   3   2   1   0
 *          +---+---+---+---+---+
 *  Cycle 1 | V | C | C | T | P |
 *          +---+---+---+---+---+
 *  Cycle 2 | S1 S0 |   Length  |
 *          +---+---+---+---+---+
 *
 * The V bit typically indicates that the command is valid. The T field
 * typically indicates that data movement is to the Pchip. The P field
 * indicates whether Pchip0 or Pchip1 is involved in the transaction.
 *
 * The full VCCT field is interpreted as outlined in Table 7-2.
 */
typedef enum
{
    AXP_No_Quadwords    = 0,
    AXP_One_Quadwords   = 1 * sizeof(u64),
    AXP_Two_Quadwords   = 2 * sizeof(u64),
    AXP_Three_Quadwords = 3 * sizeof(u64),
    AXP_Four_Quadwords  = 4 * sizeof(u64),
    AXP_Five_Quadwords  = 5 * sizeof(u64),
    AXP_Six_Quadwords   = 6 * sizeof(u64),
    AXP_Seven_Quadwords = 7 * sizeof(u64),
    AXP_Eight_Quadwords = 8 * sizeof(u64)
} AXP_PAD_Length;
typedef enum
{
    AXP_Shift_None = 0,
    AXP_Shift_One,
    AXP_Shift_Two,
    AXP_Shift_Three,
    AXP_Shift_Four,
    AXP_Shift_Five,
    AXP_Shift_Six,
    AXP_Shift_Seven
} AXP_PAD_Shift;

/*
 * Table 7-5 CPM Commands [and Timing of Data Transfer]
 *
 *  ---------------------------------------------------------------------------
 *                      Dchip
 *  Opcode  Ext Mnem    Operation
 *  ---------------------------------------------------------------------------
 *  00000   --- nop     No-op
 *  00001   --- ---     ---
 *  00010   mcc m2c     Memory bus m to CPU bus cc
 *  00011   mcc m2ca    Memory bus m to CPU bus cc  (not used)
 *  00100   -cc csr2c   Cchip CSR to CPU bus cc     (implementation specific)
 *  00101   -cc c2csr   CPU bus cc to Cchip CSR     (implementation specific)
 *  00110   mcc c2m     CPU bus cc to memory bus m
 *  00111   mcc c2m     CPU bus cc to memory bus m  (not used)
 *  01000   mm- m2p     Memory bus mm to TPQ
 *  01001   mm- pw2m    FPQ to memory bus mm and merge with WMB
 *  01010   -cc t2c     TIGbus to CPU cc            (implementation specific)
 *  01011   -cc c2t     CPU cc to TIGbus            (implementation specific)
 *  01100   -cc c2p     CPU bus cc to TPQ
 *  01101   -cc c2p     CPU bus cc to TPQ           (not used)
 *  01110   -cc h2po    Cache hit on CPU cc to TPQ
 *                      Load cache data at top of TPQ and overwrite stale
 *                      memory data
 *  01111   -cc h2pb(1) Late cache hit on CPU cc to TPQ
 *                      Decrement top of TPQ pointer, then load late cache data
 *                      at top of TPQ and overwrite stale memory data
 *  10000   dcc c2dp    CPU bus cc to TPQ
 *                      CPU bus cc to Dchip CSR b'0d'
 *  10001   dcc c2dp    CPU bus cc to TPQ           (not used)
 *                      CPU bus cc to Dchip CSR b'1d'
 *  10010   dcc dp2c    Dchip CSR b'0d' to cpu cc and discard 1 from FPQ
 *  10011   dcc dp2c    Same as above except for CSR b'1d'  (not used)
 *  10100   -cc p2c     FPQ to CPU cc
 *  10101   --- ---     ---
 *  10110   --- p2p     FPQ to TPQ
 *  10111   --- p2p     FPQ to TPQ                  (not used)
 *  11000   -aa h2c0    CPU bus aa to CPU bus 0 - used for cache hit
 *  11001   -aa h2c1    CPU bus aa to CPU bus 1 - used for cache hit
 *  11010   -aa h2c2    CPU bus aa to CPU bus 2 - used for cache hit
 *  11011   -aa h2c3    CPU bus aa to CPU bus 3 - used for cache hit
 *  11100   --- ---     ---
 *  11101   mm- m2w     Memory bus mm to WMB
 *  11110   mm- w2m     WMB to memory bus mm
 *  11111   -cc h2w     Cache hit on CPU cc to WMB
 *  ---------------------------------------------------------------------------
 *  (1) The Cchip cannot issue this command if the TPQ is full without
 *      overwriting the tail of the queue.
 *
 * NOTE: The implementation specific commands (csr2c, c2src, t2c, and c2t) do
 *       not move data between anything but the CPUs and the Cchip.  Therefore,
 *       there are no added commands to move the Cchip data to any of the other
 *       queues.
 */
typedef enum
{
    CPM_NoOp = 0,   /* b'00000' */
    CPM_m2c = 2,    /* b'00010' */
    CPM_csr2c = 4,  /* b'00100' */
    CPM_c2csr = 5,  /* b'00101' */
    CPM_c2m = 6,    /* b'00110' */
    CPM_m2p = 8,    /* b'01000' */
    CPM_pw2m = 9,   /* b'01001' */
    CPM_t2c = 10,   /* b'01001' */
    CPM_c2t = 11,   /* b'01001' */
    CPM_c2p = 12,   /* b'01100' */
    CPM_h2po = 14,  /* b'01110' */
    CPM_h2pb = 15,  /* b'01111' */
    CPM_c2dp = 16,  /* b'10000' */
    CPM_dp2c = 18,  /* b'10010' */
    CPM_p2c = 20,   /* b'10100' */
    CPM_p2p = 22,   /* b'10110' */
    CPM_h2c0 = 24,  /* b'11000' */
    CPM_h2c1 = 25,  /* b'11001' */
    CPM_h2c2 = 26,  /* b'11010' */
    CPM_h2c3 = 27,  /* b'11011' */
    CPM_m2w = 29,   /* b'11101' */
    CPM_w2m = 30,   /* b'11110' */
    CPM_h2w = 31    /* b'11111' */
} AXP_CPM_Command;

#endif /* _AXP_CPM_PAD_H_ */
