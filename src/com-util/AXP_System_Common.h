/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 08-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef SRC_COM_UTIL_AXP_SYSTEM_COMMON_H_
#define SRC_COM_UTIL_AXP_SYSTEM_COMMON_H_

#include "com-util/AXP_Common_Include.h"

#define AXP_COMMON_BLOCK_SIZE   64  /* Used for cache block sizes and buffers */
#define AXP_COMMON_BUFFER_SIZE  (AXP_COMMON_BLOCK_SIZE / sizeof(u64))

#define AXP_COMMON_MAX_CPUS     4
#define AXP_COMMON_MAX_ARRAYS   4
#define AXP_COMMON_MAX_PCHIPS   2

#define AXP_COMMON_BUFS_PER_CPU 4

#endif /* SRC_COM_UTIL_AXP_SYSTEM_COMMON_H_ */
