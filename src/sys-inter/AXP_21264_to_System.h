/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the function prototypes for the CPU to the
 *  System interface.
 *
 * Revision History:
 *
 *  V01.000 01-Jun-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21264_TO_SYSTEM_H_
#define _AXP_21264_TO_SYSTEM_H_

#include "AXP_21264_CPU.h"
#include "sys-inter/AXP_21264_21274_Common.h"

void
AXP_21264_SendToSystem(AXP_21264_CPU*, AXP_21264_SYSBUS_System*);

#endif /* _AXP_21264_TO_SYSTEM_H_ */
