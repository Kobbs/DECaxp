#
# Copyright (C) Jonathan D. Belanger 2019-2020.
# All Rights Reserved.
#
# This software is furnished under a license and may be used and copied only
# in accordance with the terms of such license and with the inclusion of the
# above copyright notice.  This software or any other copies thereof may not
# be provided or otherwise made available to any other person.  No title to
# and ownership of the software is hereby transferred.
#
# The information in this software is subject to change without notice and
# should not be construed as a commitment by the author or co-authors.
#
# The author and any co-authors assume no responsibility for the use or
# reliability of this software.
#
# Description:
#
#   This CMake file is used to build the libraries and executables for the
#   DECaxp project.
#
# Revision History:
#   V01.000 22-Dec-2019 Jonathan D. Belanger
#   Changing the code layout so that includes are in the same folder as the
#   source file associated with them.
#
#   V01.001 03-Jan-2020 Jonathan D. Belanger
#   Created a system subdirectory to house all the system general definitions
#   and code.
#

# Common Utilities
add_subdirectory(com-util)

# Alpha AXP 21264
add_subdirectory(cache)
add_subdirectory(c-box)
add_subdirectory(e-box)
add_subdirectory(f-box)
add_subdirectory(i-box)
add_subdirectory(m-box)

# System
add_subdirectory(system)
add_subdirectory(sys-inter)
add_subdirectory(c-chip)
add_subdirectory(d-chip)
add_subdirectory(p-chip)
add_subdirectory(memory)

# Devices
add_subdirectory(toyclock)
add_subdirectory(ethernet)
add_subdirectory(vhd)

add_library(cpu STATIC
    AXP_21264_CPU.c)

target_include_directories(cpu PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(DECaxp_Generate_SROM
    DECaxp_Generate_SROM.c)

target_link_libraries(DECaxp_Generate_SROM PRIVATE
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -liconv
    -lpcap)

target_include_directories(DECaxp_Generate_SROM PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(DECaxp
    DECaxp.c)

target_link_libraries(DECaxp PRIVATE
    cpu
    cbox
    ibox
    ebox
    fbox
    mbox
    cache
    system
    sysinter
    cchip
    dchip
    pchip
    pci_devices
    memory
    console
    toyclock
    comutl
    ethernet
    vhd
    -lxml2
    -lm
    -liconv
    -lpthread
    -lpcap
    ${compiler-rt})

target_include_directories(DECaxp PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

    
