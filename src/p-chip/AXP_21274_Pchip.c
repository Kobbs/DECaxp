/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  The Pchip is the interface chip between devices on the PCI bus and the rest
 *  of the system.  There can be one or two Pchips, and corresponding single or
 *  dual PCI buses, connected to the Cchip and Dchips. The Pchip performs the
 *  following functions:
 *      - Accepts requests from the Cchip by means of the CAPbus and enqueues
 *        them
 *      - Issues commands to the PCI bus based on these requests
 *      - Accepts requests from the PCI bus and enqueues them
 *      - Issues commands to the Cchip by means of the CAPbus based on these
 *        requests
 *      - Transfers data to and from the Dchips based on the above commands and
 *        requests
 *      - Buffers the data when necessary
 *      - Reports errors to the Cchip, after recording the nature of the error
 *
 * Revision History:
 *
 *  V01.000 22-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 12-May-2018 Jonathan D. Belanger
 *  Moved the Pchip CSRs to this module.
 *
 *  V01.002 19-May-2019 Jonathan D. Belanger
 *  GCC 7.4.0, and possibly earlier, turns on strict-aliasing rules by default.
 *  There are a number of issues in this module where the address of one
 *  variable is cast to extract a value in a different format.  In this module
 *  these all appear to be when trying to get the 64-bit value equivalent of
 *  the 64-bit long PC structure.  We will use shifts (in a macro) instead of
 *  the casts.
 *
 *  V01.003 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#include "com-util/AXP_Trace.h"
#include "p-chip/AXP_21274_PchipDefs.h"

/*
 * AXP_21274_ReadPCSR
 *  This function is called when a read request has come in for a Pchip CSR.
 *
 * Input Parameters:
 *  p:
 *      A pointer to the Pchip data structure from which the emulation
 *      information is maintained.
 *  msg:
 *      A pointer to the request from the Cchip that contains the CSR to be
 *      read.
 *
 * Output Parameters:
 *  None.
 *
 * Return Value:
 *  An unsigned  64-bit value read from the CSR.
 */
static u64
AXP_21274_ReadPCSR(AXP_21274_PCHIP *p, AXP_CAP_Msg *msg)
{
    u64 retVal = 0;

    /*
     * Let's go do what we came here to do.
     */
    switch (msg->csr)
    {
        case 0x00: /* WSBA0 */
            AXP_PCHIP_READ_WSBA0(retVal, p);
            break;

        case 0x01: /* WSBA1 */
            AXP_PCHIP_READ_WSBA1(retVal, p);
            break;

        case 0x02: /* WSBA2 */
            AXP_PCHIP_READ_WSBA2(retVal, p);
            break;

        case 0x03: /* WSBA3 */
            AXP_PCHIP_READ_WSBA3(retVal, p);
            break;

        case 0x04: /* WSM0 */
            AXP_PCHIP_READ_WSM0(retVal, p);
            break;

        case 0x05: /* WSM1 */
            AXP_PCHIP_READ_WSM1(retVal, p);
            break;

        case 0x06: /* WSM2 */
            AXP_PCHIP_READ_WSM2(retVal, p);
            break;

        case 0x07: /* WSM3 */
            AXP_PCHIP_READ_WSM3(retVal, p);
            break;

        case 0x08: /* TBA0 */
            AXP_PCHIP_READ_TBA0(retVal, p);
            break;

        case 0x09: /* TBA1 */
            AXP_PCHIP_READ_TBA1(retVal, p);
            break;

        case 0x0a: /* TBA2 */
            AXP_PCHIP_READ_TBA2(retVal, p);
            break;

        case 0x0b: /* TBA3 */
            AXP_PCHIP_READ_TBA3(retVal, p);
            break;

        case 0x0c: /* PCTL */
            AXP_PCHIP_READ_PCTL(retVal, p);
            break;

        case 0x0d: /* PLAT */
            AXP_PCHIP_READ_PLAT(retVal, p);
            break;

        case 0x0f: /* PERROR */
            AXP_PCHIP_READ_PERROR(retVal, p);
            break;

        case 0x10: /* PERRMASK */
            AXP_PCHIP_READ_PERRMASK(retVal, p);
            break;

        case 0x14: /* PMONCTL */
            AXP_PCHIP_READ_PMONCTL(retVal, p);
            break;

        case 0x15: /* PMONCNT */
            AXP_PCHIP_READ_PMONCNT(retVal, p);
            break;

        default:
            /* TODO: non-existent memory */
            break;
    }

    /* TODO: We need to send the data to the Dchip */

    /*
     * Return the results back to the caller.
     */
    return (retVal);
}

/*
 * This union is used to be able to map a 64-bit value, received from the
 * Dchip, to any of the possible Pchip CSRs.
 */
typedef union
{
    u64 value;
    AXP_21274_WSBAn Wsban;
    AXP_21274_WSBA3 Wsba3;
    AXP_21274_WSMn Wsmn;
    AXP_21274_TBAn Tban;
    AXP_21274_PCTL Pctl;
    AXP_21274_PLAT Plat;
    AXP_21274_PERROR Perror;
    AXP_21274_PERRMASK PerrMask;
    AXP_21274_PERRSET PerrSet;
    AXP_21274_TLBIV Tlbiv;
    AXP_21274_PMONCTL MonCtl;
    AXP_21274_SPRST SprSt;
} CSR_MAP;

/*
 * AXP_21274_WritePCSR
 *  This function is called when a write request has come in for a CSR.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the system data structure from which the emulation
 *      information is maintained.
 *  pa:
 *      The Physical Address that contains the CSR to be read.
 *  cpuID:
 *      An unsigned 32-bit value indicating the CPU requesting the read.  This
 *      is used when setting the correct ProbeEnable bit.  This can only be a
 *      value between 0 and 3.
 *
 * Output Parameters:
 *  None.
 *
 * Return Value:
 *  An unsigned  64-bit value read from the CSR.
 *
 * NOTE:    Writing to a CSR may not always cause the register to be updated.
 *          It is possible that some other action may be generated by simply
 *          executing the write operation.
 */
static void
AXP_21274_WritePCSR(AXP_21274_PCHIP *p, AXP_CAP_Msg *msg)
{
    CSR_MAP csrValue;
    u64 data[64]; /* TODO: We need to get the data from the Dchip */

    /*
     * Let's go do what we came here to do.
     */
    switch (msg->csr)
    {
        case 0x00: /* WSBA0 */
            csrValue.value = data[0] & AXP_21274_WSBAn_WMASK;
            p->wsba0 = csrValue.Wsban;
            break;

        case 0x01: /* WSBA1 */
            csrValue.value = data[0] & AXP_21274_WSBAn_WMASK;
            p->wsba1 = csrValue.Wsban;
            break;

        case 0x02: /* WSBA2 */
            csrValue.value = data[0] & AXP_21274_WSBAn_WMASK;
            p->wsba2 = csrValue.Wsban;
            break;

        case 0x03: /* WSBA3 */
            csrValue.value = data[0] & AXP_21274_WSBA3_WMASK;
            p->wsba3 = csrValue.Wsba3;
            break;

        case 0x04: /* WSM0 */
            csrValue.value = data[0] & AXP_21274_WSMn_WMASK;
            p->wsm0 = csrValue.Wsmn;
            break;

        case 0x05: /* WSM1 */
            csrValue.value = data[0] & AXP_21274_WSMn_WMASK;
            p->wsm1 = csrValue.Wsmn;
            break;

        case 0x06: /* WSM2 */
            csrValue.value = data[0] & AXP_21274_WSMn_WMASK;
            p->wsm2 = csrValue.Wsmn;
            break;

        case 0x07: /* WSM3 */
            csrValue.value = data[0] & AXP_21274_WSMn_WMASK;
            p->wsm3 = csrValue.Wsmn;
            break;

        case 0x08: /* TBA0 */
            csrValue.value = data[0] & AXP_21274_TBAn_WMASK;
            p->tba0 = csrValue.Tban;
            break;

        case 0x09: /* TBA1 */
            csrValue.value = data[0] & AXP_21274_TBAn_WMASK;
            p->tba1 = csrValue.Tban;
            break;

        case 0x0a: /* TBA2 */
            csrValue.value = data[0] & AXP_21274_TBAn_WMASK;
            p->tba2 = csrValue.Tban;
            break;

        case 0x0b: /* TBA3 */
            csrValue.value = data[0] & AXP_21274_TBAn_WMASK;
            p->tba3 = csrValue.Tban;
            break;

        case 0x0c: /* PCTL */
            csrValue.value = data[0] & AXP_21274_PCTL_WMASK;
            p->pctl = csrValue.Pctl;
            break;

        case 0x0d: /* PLAT */
            csrValue.value = data[0] & AXP_21274_PLAT_WMASK;
            p->plat = csrValue.Plat;
            break;

        case 0x0f: /* PERROR */
            /* TODO: HRM says RW, but I think it should be RO sys->p0Perror */
            break;

        case 0x10: /* PERRMASK */
            csrValue.value = data[0] & AXP_21274_PERRMASK_WMASK;
            p->perrMask = csrValue.PerrMask;
            break;

        case 0x11: /* PERRSET */
            csrValue.value = data[0] & AXP_21274_PERRSET_WMASK;
            p->perrSet = csrValue.PerrSet;
            break;

        case 0x12: /* TLBIV */
            csrValue.value = data[0] & AXP_21274_TLBIV_WMASK;
            p->tlbiv = csrValue.Tlbiv;
            break;

        case 0x13: /* TLBIA */
            /* TODO: Invalidate SG TLB sys->p0Tlbia; */
            break;

        case 0x14: /* PMONCTL */
            csrValue.value = data[0] & AXP_21274_PMONC_WMASK;
            p->pMonCtl = csrValue.MonCtl;
            break;

        case 0x20: /* SPRST */
            /* TODO: Soft PCI Reset sys->p0SprSt; */
            break;

        default:
            /* TODO: non-existent memory */
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Pchip_Init
 *  This function is called to initialize the Pchip CSRs as documented in HRM
 *  10.2 Chipset Registers.
 *
 * Input Parameters:
 *  p:
 *      A pointer to the Pchip data structure from which the emulation
 *      information is maintained.
 *  id:
 *      A value indicating the numeric identifier associated with this Pchip.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Pchip_Init(AXP_21274_PCHIP *p, u32 id)
{
    int ii;

    p->pChipID = id; /* Save the ID for this Pchip */

    /*
     * Initialize the message queues.  We do not have the data queues, since
     * we do not have a separate thread reading and writing data to and from
     * memory.  The Cchip performs this function on behalf of the CPU and the
     * Pchip performs this function on behalf of itself.
     */
    /* TODO: Need to figure out how messages go from Cchip to Pchip and Pchip to Cchip*/
//    AXP_INIT_QUE(p->prq);
//    for (ii = 0; ii < AXP_21274_CAPBUS_MQ_SIZE; ii++)
//    {
//        AXP_INIT_QUE(p->rq[ii].header);
//    }

    /*
     * Initialization for WSBA0, WSBA1,and WSBA2 (HRM Table 10-35).
     */
    p->wsba0.res_32 = 0;
    p->wsba0.addr = 0;
    p->wsba0.res_2 = 0;
    p->wsba0.sg = AXP_SG_DISABLE;
    p->wsba0.ena = AXP_ENA_DISABLE;

    p->wsba1.res_32 = 0;
    p->wsba1.addr = 0;
    p->wsba1.res_2 = 0;
    p->wsba1.sg = AXP_SG_DISABLE;
    p->wsba1.ena = AXP_ENA_DISABLE;

    p->wsba2.res_32 = 0;
    p->wsba2.addr = 0;
    p->wsba2.res_2 = 0;
    p->wsba2.sg = AXP_SG_DISABLE;
    p->wsba2.ena = AXP_ENA_DISABLE;

    /*
     * Initialization for WSBA3 (HRM Table 10-36).
     */
    p->wsba3.res_40 = 0;
    p->wsba3.dac = AXP_DAC_DISABLE;
    p->wsba3.res_32 = 0;
    p->wsba3.addr = 0;
    p->wsba3.res_2 = 0;
    p->wsba3.sg = AXP_SG_ENABLE;
    p->wsba3.ena = AXP_ENA_DISABLE;

    /*
     * Initialization for WSM0, WSM1, WSM2, and WSM3 (HRM Table 10-37).
     */
    p->wsm0.res_32 = 0;
    p->wsm0.am = 0;
    p->wsm0.res_0 = 0;

    p->wsm1.res_32 = 0;
    p->wsm1.am = 0;
    p->wsm1.res_0 = 0;

    p->wsm2.res_32 = 0;
    p->wsm2.am = 0;
    p->wsm2.res_0 = 0;

    p->wsm3.res_32 = 0;
    p->wsm3.am = 0;
    p->wsm3.res_0 = 0;

    /*
     * Initialization for TBA0, TBA1, and TBA2 (HRM Table 10-38).
     */
    p->tba0.res_35 = 0;
    p->tba0.addr = 0;
    p->tba0.res_0 = 0;

    p->tba1.res_35 = 0;
    p->tba1.addr = 0;
    p->tba1.res_0 = 0;

    p->tba2.res_35 = 0;
    p->tba2.addr = 0;
    p->tba2.res_0 = 0;

    /*
     * Initialization for TBA3 (HRM Table 10-39).
     */
    p->tba3.res_35 = 0;
    p->tba3.addr = 0;
    p->tba3.res_0 = 0;

    /*
     * Initialization for PCTL (HRM Table 10-40).
     *
     * TODO:    Initialize pid from the PID pins.
     * TODO:    Initialize rpp from the CREQRMT_L pin at system reset.
     * TODO:    Initialize pclkx from the PCI i_pclkdiv<1:0> pins.
     * TODO:    Initialize padm from a decode of the b_cap<1:0> pins.
     */
    p->pctl.res_48 = 0;
    p->pctl.pid = 0;
    p->pctl.rpp = AXP_RPP_NOT_PRESENT;
    p->pctl.ptevrfy = AXP_PTEVRFY_DISABLE;
    p->pctl.fdwdis = AXP_FDWDIS_NORMAL;
    p->pctl.fdsdis = AXP_FDSDIS_NORMAL;
    p->pctl.pclkx = AXP_PCLKX_6_TIMES;
    p->pctl.ptpmax = 2;
    p->pctl.crqmax = 1;
    p->pctl.rev = 0;
    p->pctl.cdqmax = 1;
    p->pctl.padm = 0;
    p->pctl.eccen = AXP_ECCEN_DISABLE;
    p->pctl.res_16 = 0;
    p->pctl.ppri = AXP_PPRI_LOW;
    p->pctl.prigrp = AXP_PRIGRP_PICx_LOW;
    p->pctl.arbena = AXP_ARBENA_DISABLE;
    p->pctl.mwin = AXP_MWIN_DISABLE;
    p->pctl.hole = AXP_HOLE_DISABLE;
    p->pctl.tgtlat = AXP_TGTLAT_DISABLE;
    p->pctl.chaindis = AXP_CHAINDIS_DISABLE;
    p->pctl.thdis = AXP_THDIS_NORMAL;
    p->pctl.fbtb = AXP_FBTB_DISABLE;
    p->pctl.fdsc = AXP_FDSC_ENABLE;

    /*
     * Initialization for PLAT (HRM Table 10-41).
     */
    p->plat.res_32 = 0;
    p->plat.res_16 = 0;
    p->plat.lat = 0;
    p->plat.res_0 = 0;

    /*
     * Initialization for PERROR (HRM Table 10-42).
     */
    p->perror.syn = 0;
    p->perror.cmd = AXP_CMD_DMA_READ;
    p->perror.inv = AXP_INFO_VALID;
    p->perror.addr = 0;
    p->perror.res_12 = 0;
    p->perror.cre = 0;
    p->perror.uecc = 0;
    p->perror.res_9 = 0;
    p->perror.nds = 0;
    p->perror.rdpe = 0;
    p->perror.ta = 0;
    p->perror.ape = 0;
    p->perror.sge = 0;
    p->perror.dcrto = 0;
    p->perror.perr = 0;
    p->perror.serr = 0;
    p->perror.lost = AXP_LOST_NOT_LOST;

    /*
     * Initialization for PERRMASK (HRM Table 10-43).
     */
    p->perrMask.res_12 = 0;
    p->perrMask.mask = 0;

    /*
     * Initialization for PERRSET (HRM Table 10-44).
     */
    p->perrSet.info = 0;
    p->perrSet.res_12 = 0;
    p->perrSet.set = 0;

    /*
     * Initialization for TLBIV (HRM Table 10-45).
     */
    p->tlbiv.res_28 = 0;
    p->tlbiv.dac = 0;
    p->tlbiv.res_20 = 0;
    p->tlbiv.addr = 0;
    p->tlbiv.res_0 = 0;

    /*
     * The register for TLBIA (HRM Table 10-46) is really a pseudo register.
     * As such, writes to it are ignored.  A write to this register will simply
     * invalidate the scatter-gather TLB associated with that Pchip.
     * Therefore, there is no need to define the register or initialize it.
     */

    /*
     * Initialization for PMONCTL (HRM Table 10-47).
     */
    p->pMonCtl.res_18 = 0;
    p->pMonCtl.stkdis1 = AXP_STKDIS_STICKS_1S;
    p->pMonCtl.stkdis0 = AXP_STKDIS_STICKS_1S;
    p->pMonCtl.slct1 = 0;
    p->pMonCtl.slct0 = 1;

    /*
     * Initialization for PMONCNT (HRM Table 10-48).
     */
    p->pMonCnt.cnt1 = 0;
    p->pMonCnt.cnt0 = 0;

    /*
     * Initialize the PCI Bus.  Initially, there are no devices on the PCI bus.
     */
    p->devices = 0;
    p->bus.configPortAddress.reg = 0;
    for (ii = 0; ii < AXP_PCI_MAX_DEVICES; ii++)
    {
        int jj;

        p->bus.devs[ii].present = false;
        p->bus.devs[ii].present = false;
        p->bus.devs[ii].configRead = NULL;
        p->bus.devs[ii].configWrite = NULL;
        p->bus.devs[ii].ioRead = NULL;
        p->bus.devs[ii].ioWrite = NULL;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Pchip_Register
 *  This function is called to register a PCI device.  If the deviceId is set
 *  to the maximum number of devices possible, then the next available slot
 *  will be automatically picked out and returned to the caller.
 *
 * Input Parameters:
 *  deviceId:
 *      A 32-but unsigned value to be associated with the device.  If a device
 *      has already been defined, then a bad ID will be returned to the caller.
 *      If this parameter has a value of the maximum number of devices on a
 *      bus, then the next available slot will be allocated for the new device.
 *  configRead:
 *      A pointer to the device's configuration read function.
 *  configWrite:
 *      A pointer to the device's configuration write function.
 *  ioRead:
 *      A pointer to the device's I/O read function.
 *  ioWrite:
 *      A pointer to the device's I/O write function.
 */
u32
AXP_21274_Pchip_Register(AXP_21274_PCHIP *p,
                         u32 deviceId,
                         AXP_PCI_ConfigRead configRead,
                         AXP_PCI_ConfigWrite configWrite,
                         AXP_PCI_MemIORead ioRead,
                         AXP_PCI_MemIOWrite ioWrite,
                         AXP_PCI_Reset reset)
{
    u32 retVal = deviceId;

    /*
     * If the device ID is equal to the maximum number of devices allowed, then
     * the caller is asking this code to locate an available slot for this
     * device.
     */
    if (retVal == AXP_PCI_MAX_DEVICES)
    {
        int ii;

        for (ii = 0;
             ((ii < AXP_PCI_MAX_DEVICES) &&
              (retVal == AXP_PCI_MAX_DEVICES));
             ii++)
        {
            if (p->bus.devs[ii].present == false)
            {
                retVal = ii;
            }
        }
    }

    /*
     * If we were give a device slot that is not in use or found a device slot,
     * then initialize it.
     */
    if ((retVal < AXP_PCI_MAX_DEVICES) &&
        (p->bus.devs[retVal].present == false))
    {
        p->bus.devs[retVal].present = true;
        p->bus.devs[retVal].configRead = configRead;
        p->bus.devs[retVal].configWrite = configWrite;
        p->bus.devs[retVal].ioRead = ioRead;
        p->bus.devs[retVal].ioWrite = ioWrite;
        p->bus.devs[retVal].reset = reset;
    }
    else if (p->bus.devs[retVal].present == true)
    {
        retVal = AXP_PCI_MAX_DEVICES;
    }

    /*
     * Return the value of the allocated device.
     */
    return(retVal);
}

/*
 * AXP_21274_Pchip_Main
 *  This is the main function for the Pchip.  It looks at its queues to
 *  determine if there is anything that needs to be processed from the Cchip or
 *  PCI devices.
 *
 * Input Parameters:
 *   p:
 *       A pointer to the Pchip structure for the emulated DECchip 21272/21274
 *       chipsets.
 *
 * Output Parameters:
 *   None.
 *
 * Return Value:
 *   None.
 */
void
AXP_21274_PchipMain(void *voidPtr)
{
    AXP_21274_PCHIP *p = (AXP_21274_PCHIP *) voidPtr;
    AXP_CAP_Msg *msg;

    /*
     * Log that we are starting.
     */
    if (AXP_SYS_CALL)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("Pchip p%d is starting", p->pChipID);
        AXP_TRACE_END();
    }

    /*
     * First lock the Pchip's mutex so that we can make sure to coordinate
     * access to the Pchip's queues.
     */
    pthread_mutex_lock(&p->mutex);

    /*
     * Loop until we are no longer running.
     */
    while (p->running)
    {

        /*
         * The Pchip performs the following functions:
         *
         * This first thing we need to do is wait for something to arrive to be
         * processed.
         */
        while (p->readPrq == p->writePrq)
        {
            pthread_cond_wait(&p->cond, &p->mutex);
        }

        /*
         * We have something to process.
         */
        msg = (AXP_CAP_Msg *) p->prq[p->readPrq];
        p->readPrq = (p->readPrq + 1) % AXP_21274_PCHIP_RQ_SIZE;

        /*
         * At this point, we can unlock the Pchip mutex so that other threads
         * can send requests to the Pchip.  We'll lock it before we mark the
         * request to be processed as no longer in use.
         */
        pthread_mutex_unlock(&p->mutex);

        /*
         * Determine what has been requested and make the call needed to
         * complete request.
         */
        switch (msg->cmd)
        {
            case CAP_PCI_IACK_cycle:
            case CAP_PCI_special_cycle:
                break;

            case CAP_PCI_IO_read:

                /*
                 * The following address is a "special".  It is used to read
                 * the contents of the PCI Configuration Address (offset
                 * 0x0cf8) register.
                 */
                if (msg->address == AXP_PCI_CONFIG_ADDRESS)
                {
                    /*
                     * 1) Move the data from the PCI Configuration address into
                     *    the FPD queue
                     * 2) Send a Load PADBus Upstream message to the Cchip
                     */
                }

                /*
                 * The following address is a "special".  It is used to read
                 * the configuration data for the device addressed in the PCI
                 * Configuration Address (offset 0x0cf8) register.  This is the
                 * PCI Configuration Data register (offset 0x0cfc).
                 */
                else if (msg->address == AXP_PCI_CONFIG_DATA)
                {

                    /*
                     * 1) Verify the format of the PCI Configuration Address
                     *    register (enable bit must be set).
                     * 2) If valid, locate the device and call its read
                     *    configuration callback to get the requested
                     *    configuration data.
                     * 3) If not valid, set the returned data to all 1s.
                     * 4) Put the data into the FPQ.
                     * 5) Send a Load PADBus Data Upstream message to the Cchip
                     */
                }

                /*
                 * This is for the remaining I/O Reads
                 */
                else
                {
                }
                break;

            case CAP_PCI_IO_write:

                /*
                 * The following address is a "special".  It is used to write
                 * the contents of the PCI Configuration Address (offset
                 * 0x0cf8) register.
                 */
                if (msg->address == AXP_PCI_CONFIG_ADDRESS)
                {
                    /*
                     * 1) Move the data from TPD to the PCI Configuration
                     *    address register
                     * 2) Send an ACK message to the Cchip
                     */
                }

                /*
                 * The following address is a "special".  It is used to write
                 * the configuration data for the device addressed in the PCI
                 * Configuration Address (offset 0x0cf8) register.  This is the
                 * PCI Configuration Data register (offset 0x0cfc).
                 */
                else if (msg->address == AXP_PCI_CONFIG_DATA)
                {
                    /*
                     * 1) Verify the format of the PCI Configuration Address
                     *    register (enable bit must be set).
                     * 2) If valid, locate the device and call its read
                     *    configuration callback supplying the data from the
                     *    TPD.
                     * 3) If not valid, abort the command.
                     * 4) Send an ACK message to the Cchip
                     */
                }

                /*
                 * This is for the remaining I/O Writes
                 */
                else
                {
                }
                break;

            case CAP_PCI_memory_write_PTP:
            case CAP_PCI_memory_read:
            case CAP_PCI_memory_write_CPU:
                break;

            case CAP_CSR_read:
                if (msg->c == CAP_Pchip_CSR_operation)
                {
                    AXP_21274_ReadPCSR(p, msg);
                }
                else
                {
                    /* return all 1s */
                }
                break;

            case CAP_CSR_write:
                if (msg->c == CAP_Pchip_CSR_operation)
                {
                    AXP_21274_WritePCSR(p, msg);
                }
                break;

            /*
             * The Cchip is requesting that PCI configuration data be read
             * and returned to the requester.
             *
             * This is performed by using the PCI Configuration Data register
             * (at offset 0x0cfc).  A read to this register will use the
             * information in the PCI Configuration Address register (at offset
             * 0x0cf8), to locate the PCI device and perform the read using
             * this information.
             *
             * In order for this to work a prior PCI IO Write must have been
             * performed to the PCI Configuration Address register.
             *
             * Reading from the PCI Configuration Address register behaves
             * just like reading from any PCI register.  It contents are
             * returned.
             */
            case CAP_PCI_configuration_read:
                break;

            /*
             * The Cchip is requesting that PCI configuration data be written
             * to a PCI device.
             *
             * This is performed by using the PCI Configuration Data register
             * (at offset 0x0cfc).  A write to this register will use the
             * information in the PCI Configuration Address register (at offset
             * 0x0cf8), to locate the PCI device and perform the write using
             * this information.
             *
             * In order for this to work a couple of things must have happened
             * prior to this request.  The PCI Configuration Address register
             * must have been written with a valid PCI device.  Additionally,
             * the data to be written must have been moved into the TPD queue
             * using a Load PADBus Downstream command.
             *
             * Writing to the PCI Configuration Address register behaves
             * just like reading from any PCI register.  It data is written to
             * it.
             */
            case CAP_PCI_configuration_write:
                break;

            case CAP_Load_PADbus_data_downstream:
                /*
                 * We need to move data from the TPQM/TPQP to the appropriate
                 * Pchip buffer
                 */
                break;

            case CAP_No_op:
            default:
                break;
        }

        /*
         * We are shutting down.  Since we started everything, we need to clean
         * ourself up.  The main function will be joining to all the threads it
         * created and then freeing up the memory and exiting the image.
         */
        pthread_exit(NULL);
        return;
    }
}
