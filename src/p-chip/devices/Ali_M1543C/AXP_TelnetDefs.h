/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header contains the definitions to support TELNET server code.
 *
 * Revision History:
 *
 *  V01.000 04-Jan-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 10-May-2020 Jonathan D. Belanger
 *  Moved this code to be under the p-chip.
 */
#ifndef AXP_TELNET_DEFS_H_
#define AXP_TELNET_DEFS_H_

#include "p-chip/devices/Ali_M1543C/AXP_Telnet.h"

/*
 * Function prototypes.
 */
bool
AXP_Telnet_Send(AXP_TELNET_SESSION*, u8*, int);
void
AXP_Telnet_Main(void);
void
get_State_Machines(AXP_StateMachine***, AXP_StateMachine***);

#endif /* AXP_TELNET_H_ */
