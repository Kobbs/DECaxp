/*
 * Copyright (C) Jonathan D. Belanger 2019.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains a utility program to convert tabs into spaces.
 *  It assumes that a tab is meant to represent 4 space characters and will
 *  adjust accordingly the number of spaces that are used to replace a single
 *  tab character.  The number of spaces can be anywhere between 1 and 4.
 *
 * Revision History:
 *
 *  V01.000 24-Dec-2019 Jonathan D. Belanger
 *  Initially written.
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

void cvtTabs(char *filename)
{
    FILE *fp_in;
    char new_filename[256];
    int line = 0, column, c;
    bool hasTabs = false;

    printf("Processing file %s: ", filename);
    fp_in = fopen(filename, "r");
    if (fp_in != NULL)
    {
        c = getc(fp_in);
        while ((c != EOF) && (hasTabs == false))
        {
            hasTabs = c == '\t';
            c = getc(fp_in);
        }
    }
    else
    {
        printf("Failed to open file, %s, for read\n", filename);
    }
    if (hasTabs == true)
    {
        FILE *fp_out;

        sprintf(new_filename, "old_%s", filename);
        if (rename(filename, new_filename) == 0)
        {
            fp_out = fopen(filename, "w");
            fp_in = fopen(new_filename, "r");
            if ((fp_in != NULL) || (fp_out != NULL))
            {
                bool cvt = false;

                line = 1;
                c = getc(fp_in);
                while (c != EOF)
                {
                    cvt = false;
                    if (iscntrl(c))
                    {
                        if (c == '\t')
                        {
                            int spaces = 4 - (column % 4);

                            column += spaces;
                            while (spaces--)
                            {
                                putc(' ', fp_out);
                            }
                            cvt = true;
                        }
                        else if (c == '\n')
                        {
                            line++;
                            column = 0;
                        }
                        else
                        {
                            column++;
                        }
                    }
                    else
                    {
                        column++;
                    }
                    if (cvt == false)
                    {
                        putc(c, fp_out);
                    }
                    c = getc(fp_in);
                }
                fclose(fp_in);
                fclose(fp_out);
                printf(" Converted\n");
                exit(0);
            }
            else
            {
                if (fp_in != NULL)
                {
                    fclose(fp_in);
                }
                else
                {
                    printf("Failed to open file, %s, for read\n", new_filename);
                }
                if (fp_out != NULL)
                {
                    fclose(fp_out);
                }
                else
                {
                    printf("Failed to open file, %s, for write\n", filename);
                }
            }
        }
        else
        {
            printf("Failed to rename %s file\n", filename);
        }
    }
    else
    {
        printf(" Conversion not required\n");
    }
    return;
}

void searchInDirectory(char *dirname)
{
    DIR *dir;
    struct dirent *dirp;

    dir = opendir(dirname);
    if (dir != NULL)
    {
        chdir(dirname);
        dirp = readdir(dir);
        while (dirp != NULL)
        {
            if (dirp->d_type == DT_DIR)
            {
                if ((strcmp(dirp->d_name, ".") != 0) &&
                    (strcmp(dirp->d_name, "..") != 0))
                {
                    searchInDirectory(dirp->d_name);
                }
            }
            else
            {
                cvtTabs(dirp->d_name);
            }
            dirp = readdir(dir);
        }
        chdir("..");
        closedir(dir);
    }
    else
    {
        char string[128];

        sprintf(string, "Failed to open directory '%s'", dirname);
        perror(string);
    }
}

int main()
{
    searchInDirectory("src");
    searchInDirectory("test");
    return 0;
}

